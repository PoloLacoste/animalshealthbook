 package fr.cnam.animalshealthbook.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import fr.cnam.animalshealthbook.R;

/**
 * Activity who display a confirmation dialog
 */
public class ConfirmationDialog extends DialogFragment
{
    /**
     * Implement this interface int the parent activity to receive the dialog confirmation
     */
    public interface ConfirmationListener
    {
        void onConfirmation();
    }

    private ConfirmationListener listener;
    private String title;
    private String message;

    /**
     * @param title Title of the dialog
     * @param message Message of the dialog
     * @return ConfirmationDialog with parameters
     */
    public static ConfirmationDialog newInstance(String title, String message)
    {
        ConfirmationDialog dialog = new ConfirmationDialog();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try
        {
            listener = (ConfirmationListener) requireActivity();
        }
        catch (ClassCastException e) { }

        title = getArguments().getString("title");
        message = getArguments().getString("message");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        builder.setTitle(title)
                .setPositiveButton(R.string.yes, (dialog, which) -> listener.onConfirmation())
                .setNegativeButton(R.string.no, (dialog, which) -> dialog.dismiss());

        if(message != null)
        {
            builder.setMessage(message);
        }

        return builder.create();
    }
}
