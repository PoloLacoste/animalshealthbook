package fr.cnam.animalshealthbook.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.IconsAdapter;

public class IconsDialog extends DialogFragment
{
    /**
     * Implement this interface in the parent activity to receive the selected icon
     */
    public interface SelectIconListener
    {
        void onIconSelected(int ressource);
    }

    private SelectIconListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try
        {
            listener = (SelectIconListener) requireActivity();
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException("Calling Fragment must implement SelectIconListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_icons, null);

        AppRecyclerView recyclerView = view.findViewById(R.id.iconsList);
        IconsAdapter adapter = new IconsAdapter(res -> {
            listener.onIconSelected(res);
            this.dismiss();
        });
        adapter.setHasStableIds(true);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        builder.setTitle(getString(R.string.select_icon))
                .setView(view)
                .setNegativeButton(getString(R.string.cancel), (dialog, id) -> dialog.cancel());
        return builder.create();
    }
}
