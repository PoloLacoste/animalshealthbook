package fr.cnam.animalshealthbook.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.DialogListAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.holders.SchemaHolder;
import fr.cnam.animalshealthbook.models.schema.Schema;

public class ListSelectDialog extends DialogFragment
{
    /**
     * Implement this interface in the parent activity to receive the item selected
     */
    public interface SelectItemListener
    {
        void onItemSelected(Schema schema, Class<? extends Schema> schemaClass);
    }

    private SelectItemListener listener;

    private ArrayList<Schema> list;
    private DialogListAdapter adapter;

    private Database.Collection collection;
    private HashMap<String, Object> query;
    private String title;
    private Class<? extends SchemaHolder> holder;

    /**
     * Instantiate a new dialog with parameters
     * @param collection Collection of the list
     * @param query Hashmap of key value
     * @param title Title of the dialog
     * @param holder Class<? extends SchemaHolder> holder) holder of the item to display
     * @return
     */
    public static ListSelectDialog newInstance(Database.Collection collection,
                                               HashMap<String, Object> query, String title,
                                               Class<? extends SchemaHolder> holder)
    {
        ListSelectDialog dialog = new ListSelectDialog();

        Bundle args = new Bundle();
        args.putSerializable("collection", collection);
        args.putSerializable("query", query);
        args.putString("title", title);
        args.putSerializable("holder", holder);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try
        {
            listener = (SelectItemListener) requireActivity();
        }
        catch (ClassCastException e) { }

        collection = (Database.Collection) getArguments().getSerializable("collection");
        query = (HashMap<String, Object>) getArguments().getSerializable("query");
        title = getArguments().getString("title");
        holder = (Class<? extends SchemaHolder>) getArguments().getSerializable("holder");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.list_dialog, null);

        list = new ArrayList<>();

        Database db = new Database();
        db.getSchema(collection, query, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                list.add(schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema) { }
        });

        AppRecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        adapter = new DialogListAdapter(list, listener, holder);
        adapter.setHasStableIds(true);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        builder.setTitle(title)
                .setView(view)
                .setNegativeButton(getString(R.string.cancel), (dialog, id) -> dialog.cancel());
        return builder.create();
    }
}

