package fr.cnam.animalshealthbook.activity.model;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.Arrays;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.fragment.AnimalFragment;
import fr.cnam.animalshealthbook.fragment.DiseasesFragment;
import fr.cnam.animalshealthbook.fragment.InterventionsFragment;
import fr.cnam.animalshealthbook.fragment.VaccinesFragment;
import fr.cnam.animalshealthbook.models.adapters.AnimalPageAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.schema.Animal;

/**
 * Activity for display information about one animal
 */
public class AnimalActivity extends AppCompatActivity implements ConfirmationDialog.ConfirmationListener
{
    private Animal animal;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private AnimalFragment animalFragment;
    private VaccinesFragment vaccinesFragment;
    private DiseasesFragment diseasesFragment;
    private InterventionsFragment interventionsFragment;
    private Database db;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        animal = (Animal)getIntent().getSerializableExtra("animal");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(animal.getName());
        toolbar.setNavigationOnClickListener(v -> finish());

        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tab_layout);

        animalFragment = new AnimalFragment();
        vaccinesFragment = new VaccinesFragment();
        diseasesFragment = new DiseasesFragment();
        interventionsFragment = new InterventionsFragment();

        db = new Database();

        AnimalPageAdapter adapter = new AnimalPageAdapter(this, getSupportFragmentManager(),
                Arrays.asList(animalFragment, vaccinesFragment, diseasesFragment, interventionsFragment));
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.animal_default_picture).setText(R.string.animal);
        tabLayout.getTabAt(1).setIcon(R.drawable.vaccine_1).setText(R.string.vaccines);
        tabLayout.getTabAt(2).setIcon(R.drawable.virus_1).setText(R.string.diseases);
        tabLayout.getTabAt(3).setIcon(R.drawable.intervention).setText(R.string.intervention);
    }

    @Override
    public void onDestroy()
    {
        db.unload();
        super.onDestroy();
    }

    @Override
    public void onConfirmation()
    {
        animalFragment.deleteAnimal();
    }

    /**
     * Use it for take the animal
     * @return
     */
    public Animal getAnimal()
    {
        return animal;
    }

    /**
     * Use it for take the database
     * @return
     */
    public Database getDb()
    {
        return db;
    }
}
