package fr.cnam.animalshealthbook.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.dialogs.ListSelectDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.holders.AnimalHolder;
import fr.cnam.animalshealthbook.models.holders.VeterinaryHolder;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Activity for Add one Appointment
 */
public class AddAppointmentActivity extends AppCompatActivity implements ListSelectDialog.SelectItemListener,
        ConfirmationDialog.ConfirmationListener
{
    private Calendar calendar;
    private TextView date;

    private DialogFragment dialog;

    private TextInputLayout dateLayout;
    private TextInputLayout animalLayout;
    private TextInputLayout veterinaryLayout;

    private TextView animalName;
    private ImageView animalPicture;
    private ImageView animalSex;
    private TextView animalId;

    private TextView veterinayName;
    private TextView veterinaryPhone;

    private Animal animal;
    private Veterinary veterinary;

    private Database db;

    private boolean confirmation = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_an_appointment));
        toolbar.setNavigationOnClickListener(v -> finish());

        db = new Database();

        date = findViewById(R.id.date);

        animalName = findViewById(R.id.animalName);
        animalPicture = findViewById(R.id.animalPicture);
        animalSex = findViewById(R.id.animalSex);
        animalId = findViewById(R.id.animalId);

        veterinayName = findViewById(R.id.veterinayName);
        veterinaryPhone = findViewById(R.id.phone);

        dateLayout = findViewById(R.id.layout_date);
        animalLayout = findViewById(R.id.animalLayout);
        veterinaryLayout = findViewById(R.id.veterinaryLayout);

        animalLayout.setOnClickListener(v -> animalPicker());
        veterinaryLayout.setOnClickListener(v -> veterinaryPicker());

        Button addAppointment = findViewById(R.id.addAppointment);

        date.setOnClickListener(v -> {
            if(calendar == null)
            {
                calendar = Calendar.getInstance();
            }
            datePicker();
        });
        addAppointment.setOnClickListener(v -> addAppointment());

        Appointment appointment = (Appointment) getIntent().getSerializableExtra("appointment");

        if(appointment != null)
        {
            calendar = Calendar.getInstance();
            calendar.setTime(appointment.getDate());
            date.setText(Utils.dateFormat(calendar));

            db.getSchemaByKey(Database.Collection.ANIMALS, appointment.getAnimal(), new DatabaseListener() {
                @Override
                public void onGetSchema(Schema schema)
                {
                    animal = (Animal) schema;
                    displayAnimal();
                }

                @Override
                public void onRemoveSchema(Schema schema) { }
            });

            db.getSchemaByKey(Database.Collection.VETERINARIANS, appointment.getVeterinary(), new DatabaseListener() {
                @Override
                public void onGetSchema(Schema schema)
                {
                    veterinary = (Veterinary) schema;
                    displayVeterinary();
                }

                @Override
                public void onRemoveSchema(Schema schema) { }
            });

            addAppointment.setText(getString(R.string.update));
        }
    }

    private void datePicker()
    {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, month, dayOfMonth) -> {
            calendar.set(year, month, dayOfMonth);
            date.setText(Utils.dateFormat(calendar));
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void animalPicker()
    {
        dialog = ListSelectDialog.newInstance(Database.Collection.ANIMALS,
                null, getString(R.string.select_animal), AnimalHolder.class);
        dialog.show(getSupportFragmentManager(), "AnimalDialog");
    }

    private void veterinaryPicker()
    {
        dialog = ListSelectDialog.newInstance(Database.Collection.VETERINARIANS,
                null, getString(R.string.select_a_veterinary), VeterinaryHolder.class);
        dialog.show(getSupportFragmentManager(), "VeterinaryDialog");
    }

    private void displayAnimal()
    {
        findViewById(R.id.animal).setVisibility(View.VISIBLE);
        findViewById(R.id.selectAnimal).setVisibility(View.GONE);

        animalName.setText(animal.getName());
        animalId.setText(animal.getNumId());
        animalSex.setImageResource(animal.getSexRessource());
        animal.setPicture(this, animalPicture);
    }

    private void displayVeterinary()
    {
        findViewById(R.id.veterinary).setVisibility(View.VISIBLE);
        findViewById(R.id.selectVeterinary).setVisibility(View.GONE);

        veterinayName.setText(veterinary.getName());
        veterinaryPhone.setText(veterinary.getPhone());

        db.getSchemaByKey(Database.Collection.ANIMALS, veterinary.getAnimal(), new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                animal = (Animal) schema;
                displayAnimal();
            }

            @Override
            public void onRemoveSchema(Schema schema) {

            }
        });
    }

    @Override
    public void onItemSelected(Schema schema, Class<? extends Schema> schemaClass)
    {
        if(Veterinary.class.equals(schemaClass))
        {
            confirmation = true;

            veterinary = (Veterinary) schema;
            displayVeterinary();
        }
        else if(Animal.class.equals(schemaClass))
        {
            confirmation = true;

            animal = (Animal) schema;
            displayAnimal();
        }

        dialog.dismiss();
    }

    private void addAppointment()
    {
        dateLayout.setError(null);
        animalLayout.setError(null);
        veterinaryLayout.setError(null);

        if(calendar == null)
        {
            dateLayout.setError(getString(R.string.must_select_date));
            return;
        }

        if(animal == null)
        {
            animalLayout.setError(getString(R.string.must_select_animal));
            return;
        }

        if(veterinary == null)
        {
            veterinaryLayout.setError(getString(R.string.must_select_veterinary));
            return;
        }

        Intent intent = new Intent();
        intent.putExtra("appointment", new Appointment(calendar.getTime(),
                animal.getId(), veterinary.getId()));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        if(confirmation)
        {
            ConfirmationDialog dialog = ConfirmationDialog.newInstance(getString(R.string.cancel_modifications),
                    getString(R.string.text_cancel_modifications));
            dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfirmation()
    {
        finish();
    }
}
