package fr.cnam.animalshealthbook.activity.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddDiseaseActivity;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Display information of one Disease
 */
public class DiseaseActivity extends AppCompatActivity implements ConfirmationDialog.ConfirmationListener
{
    private Disease disease;

    private Database db;

    private TextView start_date, end_date, description;

    private ConfirmationDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disease);

        disease = (Disease) getIntent().getSerializableExtra("disease");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        db = new Database();

        start_date = findViewById(R.id.start_date);
        end_date = findViewById(R.id.end_date);
        description = findViewById(R.id.description);

        setDiseaseView();
    }

    private void setDiseaseView()
    {
        getSupportActionBar().setTitle(disease.getName());

        start_date.setText(String.format("%s : %s", getString(R.string.start_date),
                Utils.dateFormat(disease.getStartDate())));

        if(disease.getEndDate() != null)
        {
            end_date.setText(String.format("%s : %s", getString(R.string.end_date),
                    Utils.dateFormat(disease.getEndDate())));
        }

        description.setText(disease.getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.animal_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.edit: editDisease(); return true;
            case R.id.delete: confirmationDelete(getString(R.string.confirm_deletion),
                    getString(R.string.sure_delete_disease));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmationDelete(String title, String description)
    {
        dialog = ConfirmationDialog.newInstance(title, description);
        dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
    }

    private void editDisease()
    {
        Intent intent = new Intent(this, AddDiseaseActivity.class);
        intent.putExtra("disease", disease);
        startActivityForResult(intent, Utils.MODIFY_DISEASE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.MODIFY_DISEASE:
                if(data != null)
                {
                    Schema schema = (Schema) data.getSerializableExtra("disease");
                    schema.setId(disease.getId());

                    db.updateSchema(Database.Collection.DISEASES, schema);

                    disease = (Disease) schema;
                    setDiseaseView();
                }
                break;
        }
    }

    @Override
    public void onConfirmation()
    {
        dialog.dismiss();
        disease.delete(db);
        finish();
    }
}
