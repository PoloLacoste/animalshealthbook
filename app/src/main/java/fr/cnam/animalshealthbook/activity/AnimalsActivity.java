package fr.cnam.animalshealthbook.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.Arrays;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.fragment.AnimalsFragment;
import fr.cnam.animalshealthbook.fragment.AppointmentsFragment;
import fr.cnam.animalshealthbook.fragment.VeterinariansFragment;
import fr.cnam.animalshealthbook.models.adapters.AnimalsPageAdapter;
import fr.cnam.animalshealthbook.models.database.Database;

/**
 * First Activity of application, display animals
 */
public class AnimalsActivity extends AppCompatActivity
{
    private Database db;

    private ViewPager viewPager;
    private TabLayout tabLayout;

    private AnimalsFragment animalsFragment;
    private AppointmentsFragment appointmentsFragment;
    private VeterinariansFragment veterinariansFragment;

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animals);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.ic_animal_book);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tab_layout);

        animalsFragment = new AnimalsFragment();
        appointmentsFragment = new AppointmentsFragment();
        veterinariansFragment = new VeterinariansFragment();

        db = new Database();

        AnimalsPageAdapter adapter = new AnimalsPageAdapter(this, getSupportFragmentManager(),
                Arrays.asList(animalsFragment, appointmentsFragment, veterinariansFragment));
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.animal_default_picture).setText(R.string.animals);
        tabLayout.getTabAt(1).setIcon(R.drawable.calendar).setText(R.string.appointments);
        tabLayout.getTabAt(2).setIcon(R.drawable.doctor).setText(R.string.veterinarians);
    }

    @Override
    public void onDestroy()
    {
        db.unload();
        super.onDestroy();
    }

    /**
     * @return the database object
     */
    public Database getDb()
    {
        return db;
    }
}