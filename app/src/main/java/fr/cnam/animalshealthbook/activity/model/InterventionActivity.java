package fr.cnam.animalshealthbook.activity.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddInterventionActivity;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Intervention;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Display information of one Intervention
 */
public class InterventionActivity extends AppCompatActivity implements ConfirmationDialog.ConfirmationListener
{
    private Intervention intervention;

    private Database db;

    private TextView comment, date;

    private CardView noLink, diseaseCard, vaccineCard;

    private ImageView vaccineImage;
    private TextView vaccineName;
    private TextView vaccineDate;

    private ImageView diseaseImage;
    private TextView diseaseName;
    private TextView diseaseDate;

    private Disease disease;
    private Vaccine vaccine;

    private ConfirmationDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervention);

        db = new Database();

        intervention = (Intervention) getIntent().getSerializableExtra("intervention");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        date = findViewById(R.id.date);
        comment = findViewById(R.id.comment);

        vaccineImage = findViewById(R.id.vaccineImage);
        vaccineName = findViewById(R.id.vaccineName);
        vaccineDate = findViewById(R.id.dateOfTaking);

        diseaseImage = findViewById(R.id.diseaseImage);
        diseaseName = findViewById(R.id.diseaseName);
        diseaseDate = findViewById(R.id.start_date);

        noLink = findViewById(R.id.noLink_card);
        diseaseCard = findViewById(R.id.disease_card);
        vaccineCard = findViewById(R.id.vaccine_card);

        diseaseCard.setOnClickListener(view -> goToDisease());
        vaccineCard.setOnClickListener(view -> goToVaccine());

        setInterventionView();
    }

    private void setInterventionView()
    {
        getSupportActionBar().setTitle(intervention.getName());

        date.setText(Utils.dateFormat(intervention.getDate()));

        if (intervention.getComment() != null)
        {
            comment.setText(intervention.getComment());
        }

        switch (intervention.getLink())
        {
            case VACCINE:
                db.getSchemaByKey(Database.Collection.VACCINES, intervention.getIdLink(), new DatabaseListener() {
                    @Override
                    public void onGetSchema(Schema schema)
                    {
                        vaccine = (Vaccine) schema;

                        vaccineImage.setImageResource(R.drawable.vaccine_1);
                        vaccineName.setText(vaccine.getName());
                        vaccineDate.setText(Utils.dateFormat(vaccine.getDateOfTaking()));

                        showVaccine();
                    }

                    @Override
                    public void onRemoveSchema(Schema schema) { }
                });
                break;
            case DISEASE:
                db.getSchemaByKey(Database.Collection.DISEASES, intervention.getIdLink(), new DatabaseListener() {
                    @Override
                    public void onGetSchema(Schema schema)
                    {
                        disease = (Disease) schema;

                        diseaseImage.setImageResource(R.drawable.virus_1);
                        diseaseName.setText(disease.getName());
                        diseaseDate.setText(Utils.dateFormat(disease.getStartDate()));

                        showDisease();
                    }

                    @Override
                    public void onRemoveSchema(Schema schema) { }
                });
                break;
            default: showNoLink(); break;
        }
    }

    private void showNoLink()
    {
        noLink.setVisibility(View.VISIBLE);
        diseaseCard.setVisibility(View.GONE);
        vaccineCard.setVisibility(View.GONE);
    }

    private void showVaccine()
    {
        noLink.setVisibility(View.GONE);
        diseaseCard.setVisibility(View.GONE);
        vaccineCard.setVisibility(View.VISIBLE);
    }

    private void showDisease()
    {
        noLink.setVisibility(View.GONE);
        diseaseCard.setVisibility(View.VISIBLE);
        vaccineCard.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.animal_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.edit: editIntervention(); return true;
            case R.id.delete: confirmationDelete(getString(R.string.confirm_deletion),
                    getString(R.string.sure_delete_intervention));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmationDelete(String title, String description)
    {
        dialog = ConfirmationDialog.newInstance(title, description);
        dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
    }

    private void editIntervention()
    {
        Intent intent = new Intent(this, AddInterventionActivity.class);
        intent.putExtra("intervention", intervention);
        startActivityForResult(intent, Utils.MODIFY_INTERVENTION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.MODIFY_INTERVENTION:
                if(data != null)
                {
                    Schema schema = (Schema) data.getSerializableExtra("intervention");
                    schema.setId(intervention.getId());

                    db.updateSchema(Database.Collection.INTERVENTIONS, schema);

                    intervention = (Intervention) schema;
                    setInterventionView();
                }
                break;
        }
    }

    @Override
    public void onConfirmation()
    {
        dialog.dismiss();
        intervention.delete(db);
        finish();
    }

    private void goToVaccine()
    {
        Intent intent = new Intent(this, VaccineActivity.class);
        intent.putExtra("vaccine", vaccine);
        startActivity(intent);
    }

    private void goToDisease()
    {
        Intent intent = new Intent(this, DiseaseActivity.class);
        intent.putExtra("disease", disease);
        startActivity(intent);
    }
}
