package fr.cnam.animalshealthbook.activity.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddAppointmentActivity;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Display information of one Appointment
 */
public class AppointmentActivity extends AppCompatActivity implements ConfirmationDialog.ConfirmationListener
{
    private Appointment appointment;
    private Animal animal;
    private Veterinary veterinary;
    private Database db;
    private ConfirmationDialog dialog;
    private TextView date;
    private ImageView animalPicture, animalSex;
    private TextView animalName, animalId;
    private TextView veterinayName, veterinaryPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        appointment = (Appointment) getIntent().getSerializableExtra("appointment");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        db = new Database();

        date = findViewById(R.id.date);

        animalPicture = findViewById(R.id.animalPicture);
        animalName = findViewById(R.id.animalName);
        animalSex = findViewById(R.id.animalSex);
        animalId = findViewById(R.id.animalId);

        veterinayName = findViewById(R.id.veterinayName);
        veterinaryPhone = findViewById(R.id.phone);

        setAppointmentView();
    }

    private void setAppointmentView()
    {
        date.setText(Utils.dateFormat(appointment.getDate()));

        db.getSchemaByKey(Database.Collection.ANIMALS, appointment.getAnimal(), new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                animal = (Animal) schema;

                animal.setPicture(AppointmentActivity.this, animalPicture);
                animalName.setText(animal.getName());
                animalId.setText(animal.getNumId());
                animalSex.setImageResource(animal.getSexRessource());

                getSupportActionBar().setTitle(String.format("%s %s",
                        getString(R.string.appointment_of), animal.getName()));

                findViewById(R.id.animalCard).setOnClickListener(v -> goToAnimal());
            }

            @Override
            public void onRemoveSchema(Schema schema) { }
        });

        db.getSchemaByKey(Database.Collection.VETERINARIANS, appointment.getVeterinary(), new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                veterinary = (Veterinary) schema;

                veterinayName.setText(veterinary.getName());
                veterinaryPhone.setText(veterinary.getPhone());

                findViewById(R.id.veterinaryCard).setOnClickListener(v -> goToVeterinary());
            }

            @Override
            public void onRemoveSchema(Schema schema) { }
        });
    }


    private void goToAnimal()
    {
        Intent intent = new Intent(this, AnimalActivity.class);
        intent.putExtra("animal", animal);
        startActivity(intent);
    }

    private void goToVeterinary()
    {
        Intent intent = new Intent(this, VeterinaryActivity.class);
        intent.putExtra("veterinary", veterinary);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.animal_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.edit: editAppointment(); return true;
            case R.id.delete: confirmationDelete(getString(R.string.confirm_deletion),
                    getString(R.string.sure_delete_appointment));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmationDelete(String title, String description)
    {
        dialog = ConfirmationDialog.newInstance(title, description);
        dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
    }

    private void editAppointment()
    {
        Intent intent = new Intent(this, AddAppointmentActivity.class);
        intent.putExtra("appointment", appointment);
        startActivityForResult(intent, Utils.MODIFY_APPOINTMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.MODIFY_APPOINTMENT:
                if(data != null)
                {
                    Schema schema = (Schema) data.getSerializableExtra("appointment");
                    schema.setId(appointment.getId());

                    db.updateSchema(Database.Collection.APPOINTMENTS, schema);

                    appointment = (Appointment) schema;
                    setAppointmentView();
                }
                break;
        }
    }

    @Override
    public void onConfirmation()
    {
        dialog.dismiss();
        appointment.delete(db);
        finish();
    }
}
