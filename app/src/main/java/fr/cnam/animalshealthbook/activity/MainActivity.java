package fr.cnam.animalshealthbook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        /*if(FirebaseAuth.getInstance().getCurrentUser() == null)
        {
            Intent intent = new Intent(this, AuthActivity.class);
            startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(this, AnimalsActivity.class);
            startActivity(intent);
        }*/
        Intent intent = new Intent(this, AnimalsActivity.class);
        startActivity(intent);
    }
}
