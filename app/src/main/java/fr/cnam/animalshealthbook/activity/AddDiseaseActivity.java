package fr.cnam.animalshealthbook.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Activity for add one Disease
 */
public class AddDiseaseActivity extends AppCompatActivity
{
    private String animal;

    private TextInputLayout layout_name;
    private TextInputLayout layout_description;
    private TextInputLayout layout_end_date;

    private TextInputEditText name;
    private TextInputEditText description;

    private TextView start_date;
    private TextView end_date;

    private Calendar start_calendar = Calendar.getInstance();
    private Calendar end_calendar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_disease);

        animal = getIntent().getStringExtra("animal");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.add_a_disease);
        toolbar.setNavigationOnClickListener(v -> finish());

        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        start_date = findViewById(R.id.start_date);
        end_date = findViewById(R.id.end_date);

        layout_name = findViewById(R.id.layout_name);
        layout_description = findViewById(R.id.layout_description);
        layout_end_date = findViewById(R.id.layout_end_date);

        Button addDisease = findViewById(R.id.addDisease);

        addDisease.setOnClickListener(v -> addDisease());
        start_date.setOnClickListener(v -> datePicker(start_date, start_calendar));
        end_date.setOnClickListener(v -> {
            if(end_calendar == null)
            {
                end_calendar = Calendar.getInstance();
            }
            datePicker(end_date, end_calendar);
        });

        Disease disease = (Disease) getIntent().getSerializableExtra("disease");

        if(disease != null)
        {
            name.setText(disease.getName());
            description.setText(disease.getDescription());
            start_calendar.setTime(disease.getStartDate());

            if(disease.getEndDate() != null)
            {
                end_calendar = Calendar.getInstance();
                end_calendar.setTime(disease.getEndDate());
                end_date.setText(Utils.dateFormat(end_calendar));
            }

            addDisease.setText(getString(R.string.update));

            animal = disease.getAnimal();
        }

        start_date.setText(Utils.dateFormat(start_calendar));
    }

    private void datePicker(TextView textView, Calendar calendar)
    {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, month, dayOfMonth) -> {
            calendar.set(year, month, dayOfMonth);
            textView.setText(Utils.dateFormat(calendar));
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    private void addDisease()
    {
        layout_name.setError(null);
        layout_description.setError(null);
        layout_end_date.setError(null);

        String sName = name.getText().toString();

        if(Utils.isEmpty(sName))
        {
            layout_name.setError(getString(R.string.enter_valid_name));
            return;
        }

        String sDescription = description.getText().toString();

        /*if(Utils.isEmpty(sDescription))
        {
            layout_description.setError(getString(R.string.enter_valid_description));
            return;
        }*/

        Date start = start_calendar.getTime();
        Date end = end_calendar == null ? null : end_calendar.getTime();

        if(end != null)
        {
            if(start.after(end))
            {
                layout_end_date.setError(getString(R.string.start_before_end_date));
                return;
            }

            if(end.after(new Date()))
            {
                layout_end_date.setError(getString(R.string.sure_predict_end_date));
                return;
            }
        }

        Intent intent = new Intent();
        intent.putExtra("disease", new Disease(sName, start, end, sDescription, animal));
        setResult(RESULT_OK, intent);
        finish();
    }
}
