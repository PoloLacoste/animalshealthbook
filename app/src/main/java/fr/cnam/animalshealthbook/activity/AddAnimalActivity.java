package fr.cnam.animalshealthbook.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.Calendar;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.dialogs.IconsDialog;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Animals;
import fr.cnam.animalshealthbook.utils.Permissions;
import fr.cnam.animalshealthbook.utils.ToastFactory;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Activity for add one Animal
 */
public class AddAnimalActivity extends AppCompatActivity implements IconsDialog.SelectIconListener
{
    private TextInputLayout idLayout;
    private TextInputEditText id;

    private TextInputLayout nameLayout;
    private TextInputEditText name;

    private TextInputLayout speciesLayout;
    private TextInputEditText species;

    private TextInputLayout raceLayout;
    private TextInputEditText race;

    private TextView date;

    private ImageView imageView;

    private RadioGroup sex;

    private Calendar calendar = Calendar.getInstance();
    private Uri imageUri;
    private File imageFile;
    private int ressource = Animals.DEFAULT_ANIMAL_PICTURE;

    private Permissions permissions;

    private Runnable permissionCallback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_animal);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_an_animal));
        toolbar.setNavigationOnClickListener(v -> finish());

        permissions = new Permissions();
        permissions.addPermissions(new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE });

        idLayout = findViewById(R.id.idLayout);
        id = findViewById(R.id.id);

        nameLayout = findViewById(R.id.nameLayout);
        name = findViewById(R.id.animalname);

        date = findViewById(R.id.dob);
        date.setText(Utils.dateFormat(calendar));
        date.setOnClickListener(view -> datePicker());

        imageView = findViewById(R.id.animalPicture);

        speciesLayout = findViewById(R.id.speciesLayout);
        species = findViewById(R.id.species);

        raceLayout = findViewById(R.id.raceLayout);
        race = findViewById(R.id.race);

        sex = findViewById(R.id.animalsex);

        Button addAnimal = findViewById(R.id.addAnimal);

        imageView.setOnClickListener(v -> openIconsDialog());
        findViewById(R.id.openGallery).setOnClickListener(v -> openGallery());
        findViewById(R.id.takePicture).setOnClickListener(v -> takePicture());
        addAnimal.setOnClickListener(v -> addAnimal());

        Animal animal = (Animal) getIntent().getSerializableExtra("animal");

        if(animal != null)
        {
            id.setText(animal.getNumId());
            name.setText(animal.getName());
            date.setText(Utils.dateFormat(animal.getDob()));
            animal.setPicture(this, imageView);
            species.setText(animal.getSpecies());
            race.setText(animal.getRace());

            switch (animal.getSex())
            {
                case FEMALE:
                    ((RadioButton) findViewById(R.id.female)).setChecked(true);
                    break;
                case UNKNOWN:
                    ((RadioButton) findViewById(R.id.unknown)).setChecked(true);
                    break;
            }

            if(animal.getFile() == null)
            {
                ressource = animal.getRes();
            }
            else
            {
                imageFile = new File(animal.getFile());
            }

            addAnimal.setText(getString(R.string.update));
        }
    }

    private void addAnimal()
    {
        if(imageUri != null && !permissions.checkPermissions(this))
        {
            permissions.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionCallback = this::addAnimal;
            return;
        }

        Animal.Sex eSex = Animal.Sex.MALE;

        switch(sex.getCheckedRadioButtonId())
        {
            case R.id.female:
                eSex = Animal.Sex.FEMALE;
                break;
            case R.id.unknown:
                eSex = Animal.Sex.UNKNOWN;
                break;
        }

        id.setError(null);
        nameLayout.setError(null);
        speciesLayout.setError(null);

        String sId = id.getText().toString();

        if(Utils.isEmpty(sId))
        {
            idLayout.setError(getString(R.string.enter_valid_id));
            return;
        }

        String sName = name.getText().toString();

        if(Utils.isEmpty(sName))
        {
            nameLayout.setError(getString(R.string.enter_valid_name));
            return;
        }

        String sSpecies = species.getText().toString();

        if(Utils.isEmpty(sSpecies))
        {
            speciesLayout.setError(getString(R.string.enter_valid_species));
            return;
        }

        String sRace = race.getText().toString();

        if(Utils.isEmpty(sRace))
        {
            raceLayout.setError(getString(R.string.enter_valid_race));
            return;
        }

        Intent intent = new Intent();

        if(imageUri == null && imageFile == null)
        {
            intent.putExtra("animal", new Animal(sId, sName, calendar.getTime(),
                    ressource, sSpecies, sRace, eSex));
        }
        else
        {
            String path = imageFile == null ?
                    Utils.getRealPathBelowVersion(this, imageUri) : imageFile.getPath();
            intent.putExtra("animal", new Animal(sId, sName, calendar.getTime(),
                    path, sSpecies, sRace, eSex));
        }

        setResult(RESULT_OK, intent);
        finish();
    }

    private void datePicker()
    {
        new DatePickerDialog(this, (datePicker, year, month, dayOfMonth) -> {
            calendar.set(year, month, dayOfMonth);
            date.setText(Utils.dateFormat(calendar));
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void openGallery()
    {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, Utils.OPEN_GALLERY);
    }

    private void openIconsDialog()
    {
        DialogFragment dialog = new IconsDialog();
        dialog.show(getSupportFragmentManager(), "IconsDialog");
    }

    private void takePicture()
    {
        if(permissions.checkPermissions(this))
        {
            imageFile = Utils.createImageFile(this);
            if(imageFile != null)
            {
                Uri uri = FileProvider.getUriForFile(this,
                        getString(R.string.fileprovider), imageFile);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, Utils.TAKE_PICTURE);
            }

        }
        else
        {
            permissions.requestPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionCallback = this::takePicture;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for(int i = 0; i < permissions.length; i++)
        {
            switch (permissions[i])
            {
                case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                    if(grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    {
                        permissionCallback.run();
                    }
                    else
                    {
                        ToastFactory.createToast(this,
                                getString(R.string.permission_required_save_picture),
                                ToastFactory.ToastStyle.Error, Toast.LENGTH_LONG);
                    }
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);

        if(resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case Utils.OPEN_GALLERY:
                    try{
                        imageUri = data.getData();
                        if(imageUri != null)
                        {
                            Glide.with(this)
                                    .asBitmap()
                                    .load(imageUri)
                                    .thumbnail(0.5f)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .placeholder(R.drawable.animal_default_picture)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);

                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        ToastFactory.createToast(this, getString(R.string.error_picture),
                                ToastFactory.ToastStyle.Error, Toast.LENGTH_LONG);
                    }
                    break;
                case Utils.TAKE_PICTURE:
                    if(imageFile != null)
                    {
                        try{
                            Glide.with(this)
                                    .asBitmap()
                                    .load(imageFile)
                                    .thumbnail(0.5f)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .placeholder(R.drawable.animal_default_picture)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imageView);

                        }
                        catch (Exception e) 
                        {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void onIconSelected(int ressource)
    {
        imageFile = null;
        imageUri = null;

        imageView.setImageResource(ressource);
        this.ressource = ressource;
    }
}
