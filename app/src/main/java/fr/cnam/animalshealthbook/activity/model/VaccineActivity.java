package fr.cnam.animalshealthbook.activity.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddVaccineActivity;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Display information of one Vaccine
 */
public class VaccineActivity extends AppCompatActivity implements ConfirmationDialog.ConfirmationListener
{
    private Vaccine vaccine;

    private TextView dateoftaking;
    private TextView dateofrenewale;
    private TextView comment;

    private Database db;

    private ConfirmationDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vaccine);

        vaccine = (Vaccine) getIntent().getSerializableExtra("vaccine");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());


        dateoftaking = findViewById(R.id.taking_date);
        dateofrenewale = findViewById(R.id.renewale_date);
        comment = findViewById(R.id.comment);

        db = new Database();

        setVaccineView();
    }

    private void setVaccineView()
    {
        getSupportActionBar().setTitle(vaccine.getName());

        dateoftaking.setText(Utils.dateFormat(vaccine.getDateOfTaking()));

        if(vaccine.getDateOfRenewale() != null)
        {
            dateofrenewale.setText(Utils.dateFormat(vaccine.getDateOfRenewale()));
        }

        if(vaccine.getComment() != null)
        {
            comment.setText(vaccine.getComment());
        }
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.animal_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.edit: editVaccine(); return true;
            case R.id.delete: confirmationDelete(getString(R.string.confirm_deletion),
                    getString(R.string.sure_delete_vaccine));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmationDelete(String title, String description)
    {
        dialog = ConfirmationDialog.newInstance(title, description);
        dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
    }

    private void editVaccine()
    {
        Intent intent = new Intent(this, AddVaccineActivity.class);
        intent.putExtra("vaccine", vaccine);
        startActivityForResult(intent, Utils.MODIFY_VACCINE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.MODIFY_VACCINE:
                if(data != null)
                {
                    Schema schema = (Schema) data.getSerializableExtra("vaccine");
                    schema.setId(vaccine.getId());

                    db.updateSchema(Database.Collection.VACCINES, schema);

                    vaccine = (Vaccine) schema;
                    setVaccineView();
                }
                break;
        }
    }

    @Override
    public void onConfirmation()
    {
        dialog.dismiss();
        vaccine.delete(db);
        finish();
    }
}
