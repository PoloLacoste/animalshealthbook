package fr.cnam.animalshealthbook.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.dialogs.ListSelectDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.holders.DiseaseHolder;
import fr.cnam.animalshealthbook.models.holders.VaccineHolder;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Intervention;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Activity for add one Intervention
 */
public class AddInterventionActivity extends AppCompatActivity implements ListSelectDialog.SelectItemListener,
        ConfirmationDialog.ConfirmationListener
{
    private String animal;
    private Vaccine vaccine;
    private Disease disease;

    private Calendar calendar;

    private Database db;

    private RadioGroup links;

    private TextView name;
    private TextView date;
    private TextView comment;

    private TextInputLayout layoutName;
    private TextInputLayout layoutDate;

    private TextInputLayout vaccineLink;
    private TextView vaccineName;
    private TextView vaccineDate;

    private TextInputLayout diseaseLink;
    private TextView diseaseName;
    private TextView diseaseDate;

    private Button addSchema;

    private DialogFragment dialog;

    private View viewVaccine, viewDisease, selectVaccine, selectDisease;

    private boolean saveVaccine = false;
    private boolean saveDisease = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_intervention);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        db = new Database();

        animal = getIntent().getStringExtra("animal");

        layoutName = findViewById(R.id.layout_name);
        layoutDate = findViewById(R.id.layout_date);

        name = findViewById(R.id.name);
        date = findViewById(R.id.date);
        comment = findViewById(R.id.comment);

        links = findViewById(R.id.links);

        links.setOnCheckedChangeListener((group, checkedId) -> {
            switch(links.getCheckedRadioButtonId())
            {
                case R.id.vaccine: showVaccine(); break;
                case R.id.disease: showDisease(); break;
                default: hideVaccine(); hideDisease();
            }
        });

        date.setOnClickListener(view -> {
            if(calendar == null)
            {
                calendar = Calendar.getInstance();
            }

            datePicker();
        });

        viewVaccine = findViewById(R.id.viewVaccine);
        viewDisease = findViewById(R.id.viewDisease);
        selectVaccine = findViewById(R.id.selectVaccine);
        selectDisease = findViewById(R.id.selectDisease);

        vaccineLink = findViewById(R.id.vaccineLink);
        vaccineName = findViewById(R.id.vaccineName);
        vaccineDate = findViewById(R.id.dateOfTaking);

        diseaseLink = findViewById(R.id.diseaseLink);
        diseaseName = findViewById(R.id.diseaseName);
        diseaseDate = findViewById(R.id.start_date);

        addSchema = findViewById(R.id.addSchema);

        Button addIntervention = findViewById(R.id.addIntervention);

        vaccineLink.setOnClickListener(v -> vaccinePicker());
        diseaseLink.setOnClickListener(v -> diseasePicker());

        addSchema.setOnClickListener(v -> {
            switch(links.getCheckedRadioButtonId())
            {
                case R.id.vaccine: goToAddVaccine(); break;
                case R.id.disease: goToAddDisease(); break;
                default: break;
            }
        });
        addIntervention.setOnClickListener(v -> addIntervention());

        Intervention intervention = (Intervention) getIntent().getSerializableExtra("intervention");

        if(intervention != null)
        {
            name.setText(intervention.getName());
            calendar = Calendar.getInstance();
            calendar.setTime(intervention.getDate());
            date.setText(Utils.dateFormat(calendar));
            comment.setText(intervention.getComment());

            switch (intervention.getLink())
            {
                case VACCINE:
                    ((RadioButton) findViewById(R.id.vaccine)).setChecked(true);
                    db.getSchemaByKey(Database.Collection.VACCINES, intervention.getIdLink(), new DatabaseListener() {
                        @Override
                        public void onGetSchema(Schema schema)
                        {
                            vaccine = (Vaccine) schema;
                            displayVaccine();
                        }

                        @Override
                        public void onRemoveSchema(Schema schema) { }
                    });
                    break;
                case DISEASE:
                    ((RadioButton) findViewById(R.id.disease)).setChecked(true);
                    db.getSchemaByKey(Database.Collection.DISEASES, intervention.getIdLink(), new DatabaseListener() {
                        @Override
                        public void onGetSchema(Schema schema)
                        {
                            disease = (Disease) schema;
                            displayDisease();
                        }

                        @Override
                        public void onRemoveSchema(Schema schema) { }
                    });
                    break;
            }

            addIntervention.setText(getString(R.string.update));

            animal = intervention.getAnimal();
        }
    }

    private void datePicker()
    {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, month, dayOfMonth) -> {
            calendar.set(year, month, dayOfMonth);
            date.setText(Utils.dateFormat(calendar));
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void showVaccine()
    {
        hideDisease();

        if(vaccine != null)
        {
            viewVaccine.setVisibility(View.VISIBLE);
            selectVaccine.setVisibility(View.GONE);
        }

        vaccineLink.setVisibility(View.VISIBLE);
        addSchema.setVisibility(View.VISIBLE);
    }

    private void showDisease()
    {
        hideVaccine();

        if(disease != null)
        {
            viewDisease.setVisibility(View.VISIBLE);
            selectDisease.setVisibility(View.GONE);
        }

        diseaseLink.setVisibility(View.VISIBLE);
        addSchema.setVisibility(View.VISIBLE);
    }

    private void hideVaccine()
    {
        vaccineLink.setVisibility(View.GONE);
        addSchema.setVisibility(View.GONE);
    }

    private void hideDisease()
    {
        diseaseLink.setVisibility(View.GONE);
        addSchema.setVisibility(View.GONE);
    }

    private void vaccinePicker()
    {
        dialog = ListSelectDialog.newInstance(Database.Collection.VACCINES,
                new HashMap<String, Object>(){{
                    put("animal", animal);
                }}, getString(R.string.select_a_vaccine), VaccineHolder.class);
        dialog.show(getSupportFragmentManager(), "VaccineDialog");
    }

    private void diseasePicker()
    {
        dialog = ListSelectDialog.newInstance(Database.Collection.DISEASES,
                new HashMap<String, Object>(){{
                    put("animal", animal);
                }}, getString(R.string.select_a_disease), DiseaseHolder.class);
        dialog.show(getSupportFragmentManager(), "DiseaseDialog");
    }

    @Override
    public void onItemSelected(Schema schema, Class<? extends Schema> schemaClass)
    {
        if(Vaccine.class.equals(schemaClass))
        {
            vaccine = (Vaccine) schema;
            displayVaccine();
        }
        else if(Disease.class.equals(schemaClass))
        {
            disease = (Disease) schema;
            displayDisease();
        }

        dialog.dismiss();
    }

    private void displayVaccine()
    {
        showVaccine();

        vaccineName.setText(vaccine.getName());
        vaccineDate.setText(String.format("%s %s", getString(R.string.taken_on),
                Utils.dateFormat(vaccine.getDateOfTaking())));
    }

    private void displayDisease()
    {
        showDisease();

        diseaseName.setText(disease.getName());

        Date end = disease.getEndDate();
        String end_msg = getResources().getString(R.string.still_seak);

        if(end != null)
        {
            diseaseDate.setTextColor(getResources().getColor(R.color.success));
            end_msg = Utils.dateFormat(end);
        }

        diseaseDate.setText(String.format("%s - %s", Utils.dateFormat(disease.getStartDate()), end_msg));
    }

    private void addIntervention()
    {
        layoutName.setError(null);
        layoutDate.setError(null);
        vaccineLink.setError(null);
        diseaseLink.setError(null);

        String sName = name.getText().toString();

        if(Utils.isEmpty(sName))
        {
            layoutName.setError(getString(R.string.enter_valid_name));
            return;
        }

        if(calendar == null)
        {
            layoutDate.setError(getString(R.string.select_date));
            return;
        }

        String sComment = comment.getText().toString();

        Intervention.Link link = Intervention.Link.EMPTY;
        String schemaId = null;

        switch(links.getCheckedRadioButtonId())
        {
            case R.id.vaccine:
                if(vaccine == null)
                {
                    vaccineLink.setError(getString(R.string.must_select_vaccine));
                    return;
                }
                else
                {
                    if(saveVaccine)
                    {
                        db.addSchema(Database.Collection.VACCINES, vaccine, schema ->
                                setIntervention(new Intervention(sName, calendar.getTime(), sComment,
                                        animal, Intervention.Link.VACCINE, schema.getId())));
                    }
                    else
                    {
                        schemaId = vaccine.getId();
                        link = Intervention.Link.VACCINE;
                    }
                }
                break;
            case R.id.disease:
                if(disease == null)
                {
                    diseaseLink.setError(getString(R.string.must_select_disease));
                    return;
                }
                else
                {
                    if(saveDisease)
                    {
                        db.addSchema(Database.Collection.DISEASES, disease, schema ->
                                setIntervention(new Intervention(sName, calendar.getTime(), sComment,
                                        animal, Intervention.Link.DISEASE, schema.getId())));
                    }
                    else
                    {
                        schemaId = disease.getId();
                        link = Intervention.Link.DISEASE;
                    }
                }
                break;
        }

        if(!saveVaccine && !saveDisease)
        {
            setIntervention(new Intervention(sName, calendar.getTime(), sComment,
                    animal, link, schemaId));
        }
    }

    private void setIntervention(Intervention intervention)
    {
        Intent intent = new Intent();
        intent.putExtra("intervention", intervention);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void goToAddVaccine()
    {
        Intent intent = new Intent(this, AddVaccineActivity.class);
        intent.putExtra("animal", animal);
        startActivityForResult(intent, Utils.ADD_VACCINE);
    }

    private void goToAddDisease()
    {
        Intent intent = new Intent(this, AddDiseaseActivity.class);
        intent.putExtra("animal", animal);
        startActivityForResult(intent, Utils.ADD_DISEASE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_VACCINE:
                if (data != null)
                {
                    vaccine = (Vaccine) data.getSerializableExtra("vaccine");
                    displayVaccine();
                    saveVaccine = true;
                }
                break;
            case Utils.ADD_DISEASE:
                if (data != null)
                {
                    disease = (Disease) data.getSerializableExtra("disease");
                    displayDisease();
                    saveDisease = true;
                }
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        if(saveDisease || saveVaccine)
        {
            ConfirmationDialog dialog = ConfirmationDialog.newInstance(getString(R.string.cancel_modifications),
                    getString(R.string.text_cancel_modifications));
            dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfirmation()
    {
        finish();
    }
}
