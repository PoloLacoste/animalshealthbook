package fr.cnam.animalshealthbook.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Activity for add one Vaccine
 */
public class AddVaccineActivity extends AppCompatActivity
{
    private TextInputLayout layout_name;
    private TextInputLayout layout_dateOfTaking;
    private TextInputLayout layout_dateOfRenewale;
    private TextInputLayout layout_comment;

    private TextView name;
    private TextView comment;
    private TextView dateOfTaking;
    private TextView dateOfRenewale;

    private Calendar calendarOfTaking = Calendar.getInstance();
    private Calendar calendarOfRenewale;

    private String animal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vaccine);

        animal = getIntent().getStringExtra("animal");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_a_vaccine));
        toolbar.setNavigationOnClickListener(v -> finish());

        name = findViewById(R.id.name);
        dateOfTaking = findViewById(R.id.dateOfTaking);
        dateOfRenewale = findViewById(R.id.dateOfRenewal);
        comment = findViewById(R.id.comment);

        layout_name = findViewById(R.id.layout_name);
        layout_dateOfTaking = findViewById(R.id.layout_dateOfTaking);
        layout_dateOfRenewale = findViewById(R.id.layout_dateOfRenewal);
        layout_comment = findViewById(R.id.layout_comment);

        dateOfTaking.setOnClickListener(view -> datePicker(dateOfTaking, calendarOfTaking));
        dateOfRenewale.setOnClickListener(view -> {
            if(calendarOfRenewale == null)
            {
                calendarOfRenewale = Calendar.getInstance();
            }
            datePicker(dateOfRenewale, calendarOfRenewale);
        });

        Button addVaccine = findViewById(R.id.addVaccine);

        addVaccine.setOnClickListener(view -> addVaccine());

        Vaccine vaccine = (Vaccine) getIntent().getSerializableExtra("vaccine");

        if(vaccine != null)
        {
            name.setText(vaccine.getName());
            calendarOfTaking.setTime(vaccine.getDateOfTaking());

            if(vaccine.getDateOfRenewale() != null)
            {
                calendarOfRenewale = Calendar.getInstance();
                calendarOfRenewale.setTime(vaccine.getDateOfRenewale());
                dateOfRenewale.setText(Utils.dateFormat(calendarOfRenewale));
            }

            comment.setText(vaccine.getComment());

            animal = vaccine.getAnimal();

            addVaccine.setText(getString(R.string.update));
        }

        dateOfTaking.setText(Utils.dateFormat(calendarOfTaking));
    }

    private void datePicker(TextView textView, Calendar calendar)
    {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, (datePicker, year, month, dayOfMonth) -> {
            calendar.set(year, month, dayOfMonth);
            textView.setText(Utils.dateFormat(calendar));
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        datePickerDialog.show();
    }

    private void addVaccine()
    {
        layout_name.setError(null);
        layout_dateOfTaking.setError(null);
        layout_dateOfRenewale.setError(null);
        layout_comment.setError(null);

        String sName = name.getText().toString();

        if(sName.replace(" ", "").isEmpty())
        {
            layout_name.setError(getString(R.string.enter_valid_name));
            return;
        }

        if(calendarOfTaking == null)
        {
            layout_dateOfTaking.setError(getString(R.string.select_taking_date));
            return;
        }

        Date date_renewal = calendarOfRenewale == null ? null : calendarOfRenewale.getTime();
        String sComment = comment.getText().toString();

        Intent intent = new Intent();
        intent.putExtra("vaccine", new Vaccine(sName, calendarOfTaking.getTime(),
                date_renewal, sComment, animal));
        setResult(RESULT_OK, intent);
        finish();
    }
}
