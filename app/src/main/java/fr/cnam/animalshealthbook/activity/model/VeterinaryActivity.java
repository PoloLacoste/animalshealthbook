package fr.cnam.animalshealthbook.activity.model;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddVeterinaryActivity;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Display information of one Veterinary
 */
public class VeterinaryActivity extends AppCompatActivity implements ConfirmationDialog.ConfirmationListener
{
    private Veterinary veterinary;

    private Database db;

    private ConfirmationDialog dialog;

    private TextView name, address, phone, email;

    private CardView animalCard;
    private ImageView animalPicture, animalSex;
    private TextView animalName, animalId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veterinary);

        veterinary = (Veterinary) getIntent().getSerializableExtra("veterinary");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());

        db = new Database();

        name = findViewById(R.id.name);
        address = findViewById(R.id.address);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);

        animalCard = findViewById(R.id.animalCard);
        animalPicture = findViewById(R.id.animalPicture);
        animalName = findViewById(R.id.animalName);
        animalSex = findViewById(R.id.animalSex);
        animalId = findViewById(R.id.animalId);

        setVeterinaryView();
    }

    private void setVeterinaryView()
    {
        getSupportActionBar().setTitle(veterinary.getName());

        name.setText(veterinary.getName());
        address.setText(veterinary.getAddress());
        phone.setText(veterinary.getPhone());
        email.setText(veterinary.getMail());

        if(veterinary.getAnimal() != null)
        {
            db.getSchemaByKey(Database.Collection.ANIMALS, veterinary.getAnimal(), new DatabaseListener() {
                @Override
                public void onGetSchema(Schema schema)
                {
                    findViewById(R.id.animal).setVisibility(View.VISIBLE);
                    findViewById(R.id.noAnimal).setVisibility(View.GONE);

                    Animal animal = (Animal) schema;

                    animal.setPicture(VeterinaryActivity.this, animalPicture);
                    animalName.setText(animal.getName());
                    animalId.setText(animal.getNumId());
                    animalSex.setImageResource(animal.getSexRessource());

                    animalCard.setOnClickListener(v -> goToAnimal(animal));
                }

                @Override
                public void onRemoveSchema(Schema schema) {

                }
            });
        }
        else
        {
            findViewById(R.id.animal).setVisibility(View.GONE);
            findViewById(R.id.noAnimal).setVisibility(View.VISIBLE);
        }
    }

    private void goToAnimal(Animal animal)
    {
        Intent intent = new Intent(this, AnimalActivity.class);
        intent.putExtra("animal", animal);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu)
    {
        getMenuInflater().inflate(R.menu.animal_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.edit: editVeterinary(); return true;
            case R.id.delete: confirmationDelete(getString(R.string.confirm_deletion),
                    getString(R.string.sure_delete_veterinary));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmationDelete(String title, String description)
    {
        dialog = ConfirmationDialog.newInstance(title, description);
        dialog.show(getSupportFragmentManager(), "ConfirmationDialog");
    }

    private void editVeterinary()
    {
        Intent intent = new Intent(this, AddVeterinaryActivity.class);
        intent.putExtra("veterinary", veterinary);
        startActivityForResult(intent, Utils.MODIFY_VETERINARY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.MODIFY_VETERINARY:
                if(data != null)
                {
                    Schema schema = (Schema) data.getSerializableExtra("veterinary");
                    schema.setId(veterinary.getId());

                    db.updateSchema(Database.Collection.VETERINARIANS, schema);

                    veterinary = (Veterinary) schema;
                    setVeterinaryView();
                }
                break;
        }
    }

    @Override
    public void onConfirmation()
    {
        dialog.dismiss();
        veterinary.delete(db);
        finish();
    }
}
