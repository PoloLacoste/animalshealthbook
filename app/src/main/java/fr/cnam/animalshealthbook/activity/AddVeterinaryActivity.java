package fr.cnam.animalshealthbook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.dialogs.ListSelectDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.holders.AnimalHolder;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Activity for add one Veterinary
 */
public class AddVeterinaryActivity extends AppCompatActivity implements ListSelectDialog.SelectItemListener

{
    private TextInputLayout nameLayout;
    private TextInputLayout addressLayout;
    private TextInputLayout phoneLayout;
    private TextInputLayout emailLayout;

    private TextInputEditText name;
    private TextInputEditText address;
    private TextInputEditText phone;
    private TextInputEditText email;

    private TextView animalName, animalId;
    private ImageView animalPicture, animalSex;

    private Animal animal;

    private DialogFragment animalDialog;

    private Database db;

    private boolean saveAnimal = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_veterinary);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.add_a_veterinary));
        toolbar.setNavigationOnClickListener(v -> finish());

        db = new Database();

        nameLayout = findViewById(R.id.nameLayout);
        addressLayout = findViewById(R.id.addressLayout);
        phoneLayout = findViewById(R.id.phoneLayout);
        emailLayout = findViewById(R.id.emailLayout);

        name = findViewById(R.id.name);
        address = findViewById(R.id.address);
        phone = findViewById(R.id.phone);
        email = findViewById(R.id.email);

        animalName = findViewById(R.id.animalName);
        animalId = findViewById(R.id.animalId);
        animalPicture = findViewById(R.id.animalPicture);
        animalSex = findViewById(R.id.animalSex);

        Button addVeterinary = findViewById(R.id.addVeterinary);

        findViewById(R.id.selectAnimal).setOnClickListener(v -> pickAnimal());
        findViewById(R.id.animal).setOnClickListener(v -> pickAnimal());
        findViewById(R.id.addAnimal).setOnClickListener(v -> goToAddAnimal());
        addVeterinary.setOnClickListener(v -> addVeterinary());

        Veterinary veterinary = (Veterinary) getIntent().getSerializableExtra("veterinary");

        if(veterinary != null)
        {
            getSupportActionBar().setTitle(String.format("%s %s", getString(R.string.update),
                    veterinary.getName()));

            name.setText(veterinary.getName());
            address.setText(veterinary.getAddress());
            phone.setText(veterinary.getPhone());
            email.setText(veterinary.getMail());

            if(veterinary.getAnimal() != null)
            {
                db.getSchemaByKey(Database.Collection.ANIMALS, veterinary.getAnimal(), new DatabaseListener() {
                    @Override
                    public void onGetSchema(Schema schema)
                    {
                        animal = (Animal) schema;
                        displayAnimal();
                    }

                    @Override
                    public void onRemoveSchema(Schema schema) { }
                });
            }

            addVeterinary.setText(getString(R.string.update));
        }
    }

    private void pickAnimal()
    {
        animalDialog = ListSelectDialog.newInstance(Database.Collection.ANIMALS,
                null, getString(R.string.select_animal), AnimalHolder.class);
        animalDialog.show(getSupportFragmentManager(), "AnimalDialog");
    }

    private void addVeterinary()
    {
        nameLayout.setError(null);
        addressLayout.setError(null);
        phoneLayout.setError(null);
        emailLayout.setError(null);

        String sName = name.getText().toString();

        if(Utils.isEmpty(sName))
        {
            nameLayout.setError(getString(R.string.enter_valid_name));
            return;
        }

        String sAddress = address.getText().toString();

        if(Utils.isEmpty(sAddress))
        {
            addressLayout.setError(getString(R.string.must_enter_address));
            return;
        }

        String sPhone = phone.getText().toString();

        if(Utils.isEmpty(sPhone) || !Patterns.PHONE.matcher(sPhone).matches())
        {
            phoneLayout.setError(getString(R.string.enter_valid_phone));
            return;
        }

        String sEmail = email.getText().toString();

        if(Utils.isEmpty(sEmail) || !Patterns.EMAIL_ADDRESS.matcher(sEmail).matches())
        {
            emailLayout.setError(getString(R.string.enter_valid_email));
            return;
        }

        if(saveAnimal && animal != null)
        {
            db.addSchema(Database.Collection.ANIMALS, animal, schema -> {
                Intent intent = new Intent();
                intent.putExtra("veterinary", new Veterinary(sName, sAddress, sPhone, sEmail,
                        animal == null ? null : schema.getId()));
                setResult(RESULT_OK, intent);
                finish();
            });
        }
        else
        {
            Intent intent = new Intent();
            intent.putExtra("veterinary", new Veterinary(sName, sAddress, sPhone, sEmail,
                    animal == null ? null : animal.getId()));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void goToAddAnimal()
    {
        Intent intent = new Intent(this, AddAnimalActivity.class);
        startActivityForResult(intent, Utils.ADD_ANIMAL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_ANIMAL:
                if (data != null)
                {
                    animal = (Animal) data.getSerializableExtra("animal");
                    displayAnimal();
                    saveAnimal = true;
                }
                break;
        }
    }

    @Override
    public void onItemSelected(Schema schema, Class<? extends Schema> schemaClass)
    {
        animal = (Animal) schema;

        displayAnimal();

        animalDialog.dismiss();
    }

    private void displayAnimal()
    {
        findViewById(R.id.animal).setVisibility(View.VISIBLE);
        findViewById(R.id.selectAnimal).setVisibility(View.GONE);

        animalName.setText(animal.getName());
        animalId.setText(animal.getNumId());

        this.animal.setPicture(this, animalPicture);
        animalSex.setImageResource(animal.getSexRessource());
    }
}
