package fr.cnam.animalshealthbook.models.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.models.database.Database;

/**
 * Model of disease
 */
public class Disease extends Schema
{
    private String id;
    private String name;
    private Date startDate;
    private Date endDate;
    private String description;
    private String animal;

    /**
     * Constructor
     * @param id Database id
     * @param name name of disease
     * @param startDate start date of disease
     * @param endDate end date of disease
     * @param description description of disease
     * @param animal animal concerned
     */
    public Disease(String id, String name, Date startDate, Date endDate, String description, String animal)
    {
        this(name, startDate, endDate, description, animal);
        this.id = id;
    }

    /**
     * Constructor
     * @param name name of disease
     * @param startDate start date of disease
     * @param endDate end date of disease
     * @param description description of disease
     * @param animal animal concerned
     */
    public Disease(String name, Date startDate, Date endDate, String description, String animal)
    {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
        this.animal = animal;
    }

    /**
     * Get the database id
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Get the name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the start date of the disease
     * @return
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Get the end date of the disease
     * @return
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * get the description
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the animal concerned
     * @return
     */
    public String getAnimal() {
        return animal;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public HashMap<String, Object> toMap()
    {
        return new HashMap<String, Object>() {{
            put("name", name);
            put("start", startDate.getTime());
            put("end", endDate == null ? -1 : endDate.getTime());
            put("description", description);
            put("animal", animal);
        }};
    }

    @Override
    public void delete(Database db)
    {
        db.removeSchema(Database.Collection.INTERVENTIONS, new HashMap<String, Object>(){{
            put("idLink", id);
        }});

        db.removeSchemaByKey(Database.Collection.DISEASES, id);
    }

    /**
     * Use it to convert a map into disease
     * @param map
     * @return
     */
    public static Schema fromMap(Map<String, Object> map)
    {
        long end = (long)map.get("end");
        Date end_date = end == -1 ? null : new Date(end);

        return new Disease(
                (String)map.get("id"),
                (String)map.get("name"),
                new Date((long)map.get("start")),
                end_date,
                (String)map.get("description"),
                (String)map.get("animal")
        );
    }
}
