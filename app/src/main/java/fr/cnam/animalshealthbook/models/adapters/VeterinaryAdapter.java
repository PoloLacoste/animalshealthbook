package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.model.VeterinaryActivity;
import fr.cnam.animalshealthbook.models.holders.VeterinaryHolder;
import fr.cnam.animalshealthbook.models.schema.Veterinarians;
import fr.cnam.animalshealthbook.models.schema.Veterinary;

/**
 * Adapter to display veterinary in a list
 */
public class VeterinaryAdapter extends RecyclerView.Adapter<VeterinaryHolder>
{
    private Veterinarians veterinarians;

    /**
     * Constructor
     * @param veterinarians List of veterinarians
     */
    public VeterinaryAdapter(Veterinarians veterinarians)
    {
        this.veterinarians = veterinarians;
    }

    @NonNull
    @Override
    public VeterinaryHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new VeterinaryHolder(inflater.inflate(R.layout.veterinary_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VeterinaryHolder veterinaryHolder, int pos)
    {
        Veterinary veterinary = (Veterinary) veterinarians.values().toArray()[pos];

        veterinaryHolder.setSchema(veterinary);

        veterinaryHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(veterinaryHolder.itemView.getContext(), VeterinaryActivity.class);
            intent.putExtra("veterinary", veterinary);

            veterinaryHolder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return veterinarians.size();
    }

    @Override
    public long getItemId(int pos)
    {
        return veterinarians.values().toArray()[pos].hashCode();
    }
}
