package fr.cnam.animalshealthbook.models.schema;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.models.database.Database;

public abstract class Schema implements Serializable
{
    Schema() {}

    public abstract String getId();
    public abstract void setId(String id);

    public abstract void delete(Database db);

    public abstract HashMap<String, Object> toMap();
    static Schema fromMap(Map<String, Object> map) { return null; }
}
