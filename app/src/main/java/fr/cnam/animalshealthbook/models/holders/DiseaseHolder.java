package fr.cnam.animalshealthbook.models.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Diseases;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Holder for display one disease
 */
public class DiseaseHolder extends SchemaHolder
{
    private Disease disease;

    private ImageView imageView;
    private TextView name;
    private TextView start_date;

    private Context context;

    /**
     * constructor of the holder
     * @param itemView
     */
    public DiseaseHolder(@NonNull View itemView)
    {
        super(itemView);

        imageView = itemView.findViewById(R.id.diseaseImage);
        name = itemView.findViewById(R.id.diseaseName);
        start_date = itemView.findViewById(R.id.start_date);

        context = itemView.getContext();
    }

    @Override
    public void setSchema(Schema schema)
    {
        disease = (Disease) schema;

        Date end = disease.getEndDate();
        String end_msg = context.getResources().getString(R.string.still_seak);

        if(end != null)
        {
            start_date.setTextColor(context.getResources().getColor(R.color.success));
            end_msg = Utils.dateFormat(end);
        }

        imageView.setImageResource(Diseases.randomRessource());
        name.setText(disease.getName());
        start_date.setText(String.format("%s - %s", Utils.dateFormat(disease.getStartDate()), end_msg));
    }

    /**
     * Use it for get the layout
     * @return
     */
    public static int getLayout()
    {
        return R.layout.disease_list_item;
    }
}
