package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.model.AnimalActivity;
import fr.cnam.animalshealthbook.models.holders.AnimalHolder;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Animals;

/**
 * Adapter to display animals in a list
 */
public class AnimalsAdapter extends RecyclerView.Adapter<AnimalHolder>
{
    private Animals animals;

    /**
     * Constructor
     * @param animals List of Animal
     */
    public AnimalsAdapter(Animals animals)
    {
        this.animals = animals;
    }

    @NonNull
    @Override
    public AnimalHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new AnimalHolder(inflater.inflate(R.layout.animal_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalHolder animalHolder, int pos)
    {
        Animal animal = (Animal) animals.values().toArray()[pos];

        animalHolder.setSchema(animal);

        animalHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(animalHolder.itemView.getContext(), AnimalActivity.class);
            intent.putExtra("animal", animal);

            animalHolder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public void onViewRecycled(@NonNull AnimalHolder holder)
    {
        super.onViewRecycled(holder);

        holder.recycle();
    }

    @Override
    public int getItemCount()
    {
        return animals.size();
    }

    @Override
    public long getItemId(int pos)
    {
        return animals.values().toArray()[pos].hashCode();
    }
}
