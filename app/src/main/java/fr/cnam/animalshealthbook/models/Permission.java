package fr.cnam.animalshealthbook.models;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Useful class to manage permission
 */
public class Permission
{
    private String name;
    private String logName;

    public Permission(String name)
    {
        this.name = name;
        this.logName = name.substring(name.lastIndexOf('.') + 1);
    }

    /**
     *
     * @param context Context of the app
     * @return if the permission is granted
     */
    public boolean checkPermission(Context context)
    {
        if(ActivityCompat.checkSelfPermission(context, name) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        }
        return false;
    }

    /**
     * Request this permission
     * @param activity Activity
     */
    public void requestPermission(Activity activity)
    {
        ActivityCompat.requestPermissions(activity, new String[]{name}, Utils.REQUEST_PERMISSIONS);
    }

    public String getName() {
        return name;
    }

    public String getLogName() {
        return logName;
    }
}
