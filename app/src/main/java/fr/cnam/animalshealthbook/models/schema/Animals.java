package fr.cnam.animalshealthbook.models.schema;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import fr.cnam.animalshealthbook.R;

/**
 * List of default type of animals
 */
public class Animals extends HashMap<String, Animal>
{
    /**
     * default icon
     */
    public static final int DEFAULT_ANIMAL_PICTURE = R.drawable.animal_default_picture;
    /**
     * List of icons by default for animals
     */
    public static final List<Integer> Ressources = Arrays.asList(
            R.drawable.cat,
            R.drawable.dog,
            R.drawable.fox,
            R.drawable.frog,
            R.drawable.hamster,
            R.drawable.mouse,
            R.drawable.parrot,
            R.drawable.snake,
            R.drawable.squirrel,
            R.drawable.rabbit,
            R.drawable.animal_default_picture
    );

    /**
     * get a random picture
     * @return
     */
    public static int randomRessource()
    {
        return Ressources.get(new Random().nextInt(Ressources.size()));
    }
}
