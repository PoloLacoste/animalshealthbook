package fr.cnam.animalshealthbook.models.database;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Intervention;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.models.schema.Veterinary;

/**
 *
 */
public class Database implements IDatabase
{
    private FirebaseFirestore db;
    private List<ListenerRegistration> listeners;

    public enum Collection
    {
        ANIMALS(Animal.class),
        APPOINTMENTS(Appointment.class),
        DISEASES(Disease.class),
        INTERVENTIONS(Intervention.class),
        VACCINES(Vaccine.class),
        VETERINARIANS(Veterinary.class);

        private Class<? extends Schema> schemaClass;

        Collection(Class<? extends Schema> schemaClass)
        {
            this.schemaClass = schemaClass;
        }

        public Class getSchemaClass()
        {
            return schemaClass;
        }
    }

    public Database()
    {
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
                .build();
        db.setFirestoreSettings(settings);
        db.disableNetwork();

        listeners = new ArrayList<>();
    }

    @Override
    public void addSchema(Collection collection, Schema schema, SimpleDatabaseListener listener)
    {
        Map<String, Object> map = schema.toMap();

        if(map != null)
        {
            DocumentReference newSchema = db.collection(collection.name()).document();
            map.put("id", newSchema.getId());

            newSchema.set(map);

            if(listener != null)
            {
                newSchema.get().addOnCompleteListener(task -> {
                    if(task.isSuccessful() && task.getResult() != null)
                    {
                        try{
                            Method method = collection.getSchemaClass().getMethod("fromMap", Map.class);
                            listener.onGetSchema((Schema)method.invoke(null, task.getResult().getData()));
                        }
                        catch (Exception ex) { }
                    }
                });
            }
        }
    }

    @Override
    public void updateSchema(Collection collection, Schema schema)
    {
        db.collection(collection.name()).document(schema.getId()).update(schema.toMap());
    }

    @Override
    public void removeSchemaByKey(Collection collection, String schemaId)
    {
        db.collection(collection.name()).document(schemaId).delete();
    }

    @Override
    public void removeSchema(Collection collection, Map<String, Object> queryMap)
    {
        Query query = getQuery(collection, queryMap);

        if(query != null)
        {
            query.get().addOnCompleteListener(task -> {
                if(task.isSuccessful())
                {
                    for (DocumentSnapshot document : task.getResult().getDocuments())
                    {
                        document.getReference().delete();
                    }
                }
            });
        }
    }

    @Override
    public void getSchemaByKey(Collection collection, String schemaId, DatabaseListener listener)
    {
        listeners.add(db.collection(collection.name()).document(schemaId).addSnapshotListener((snapshot, e) -> {
            if(snapshot != null)
            {
                try{
                    Method method = collection.getSchemaClass().getMethod("fromMap", Map.class);
                    listener.onGetSchema((Schema)method.invoke(null, snapshot.getData()));
                }
                catch (Exception ex) { }
            }
        }));
    }

    @Override
    public void getSchema(Collection collection, Map<String, Object> queryMap, DatabaseListener listener)
    {
        listeners.add(getQuery(collection, queryMap).addSnapshotListener((snapshot, e) -> {
            if(snapshot != null)
            {
                for(DocumentChange dc : snapshot.getDocumentChanges())
                {
                    switch (dc.getType())
                    {
                        case ADDED:
                        case MODIFIED:
                            try{
                                Method method = collection.getSchemaClass().getMethod("fromMap", Map.class);
                                listener.onGetSchema((Schema)method.invoke(null, dc.getDocument().getData()));
                            }
                            catch (Exception ex)
                            {
                                ex.printStackTrace();
                            }
                            break;
                        case REMOVED:
                            try{
                                Method method = collection.getSchemaClass().getMethod("fromMap", Map.class);
                                listener.onRemoveSchema((Schema)method.invoke(null, dc.getDocument().getData()));
                            }
                            catch (Exception ex) { }
                            break;
                    }
                }
            }
        }));
    }

    @Override
    public void unload()
    {
        for(ListenerRegistration listener : listeners)
        {
            listener.remove();
        }
    }

    private Query getQuery(Collection collection, Map<String, Object> queryMap)
    {
        if(queryMap == null)
        {
            return db.collection(collection.name());
        }

        Query query = null;

        for(Map.Entry<String, Object> pair : queryMap.entrySet())
        {
            if(query == null)
            {
                query = db.collection(collection.name())
                        .whereEqualTo(pair.getKey(), pair.getValue());
            }
            else
            {
                query.whereEqualTo(pair.getKey(), pair.getValue());
            }
        }

        return query;
    }
}
