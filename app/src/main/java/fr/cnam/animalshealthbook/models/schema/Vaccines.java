package fr.cnam.animalshealthbook.models.schema;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import fr.cnam.animalshealthbook.R;

/**
 * List of vaccines
 */
public class Vaccines extends HashMap<String, Vaccine>
{
    /**
     * List of vaccine icon
     */
    public static final List<Integer> Ressources = Arrays.asList(
            R.drawable.vaccine_1,
            R.drawable.vaccine_2
    );

    /**
     * get a random icon
     * @return
     */
    public static int randomRessource()
    {
        return Ressources.get(new Random().nextInt(Ressources.size()));
    }
}
