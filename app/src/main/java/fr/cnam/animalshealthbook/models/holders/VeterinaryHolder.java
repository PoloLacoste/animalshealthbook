package fr.cnam.animalshealthbook.models.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;

/**
 * Holder for display one Veterinary
 */
public class VeterinaryHolder extends SchemaHolder
{
    private Veterinary veterinary;

    private ImageView imageView;
    private TextView name;
    private TextView phone;

    private Context context;

    /**
     * constructor of the holder
     * @param itemView
     */
    public VeterinaryHolder(@NonNull View itemView)
    {
        super(itemView);

        imageView = itemView.findViewById(R.id.imageView);
        name = itemView.findViewById(R.id.veterinayName);
        phone = itemView.findViewById(R.id.phone);

        context = itemView.getContext();
    }

    @Override
    public void setSchema(Schema schema)
    {
        veterinary = (Veterinary) schema;

        name.setText(veterinary.getName());
        phone.setText(veterinary.getPhone());
    }

    /**
     * Use it for get the layout
     * @return
     */
    public static int getLayout()
    {
        return R.layout.veterinary_list_item;
    }
}
