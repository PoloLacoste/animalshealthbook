package fr.cnam.animalshealthbook.models.holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import fr.cnam.animalshealthbook.models.schema.Schema;

public abstract class SchemaHolder extends RecyclerView.ViewHolder
{
    /**
     * constructor of the holder
     * @param itemView
     */
    public SchemaHolder(@NonNull View itemView)
    {
        super(itemView);
    }

    public abstract void setSchema(Schema schema);
}
