package fr.cnam.animalshealthbook.models.database;

import java.util.Map;

import fr.cnam.animalshealthbook.models.schema.Schema;

public interface IDatabase
{
    /**
     * Add a schema in a collection of the database
     * @param collection Collection in the database
     * @param schema A schema
     * @param listener The listener to recover the added element in the database
     */
    void addSchema(Database.Collection collection, Schema schema, SimpleDatabaseListener listener);

    /**
     * Update a schema in a collection of the database
     * @param collection Collection in the database
     * @param schema A schema
     */
    void updateSchema(Database.Collection collection, Schema schema);

    /**
     * Remove a schema in a collection of the database with his ID
     * @param collection Collection in the database
     * @param schemaId Id of the schema in the database
     */
    void removeSchemaByKey(Database.Collection collection, String schemaId);

    /**
     * Remove a schema in a collection of the database
     * @param collection Collection in the database
     * @param queryMap Map of key / value (like where in SQL)
     */
    void removeSchema(Database.Collection collection, Map<String, Object> queryMap);

    /**
     * Get a schema in a collection of the database with his ID
     * @param collection Collection in the database
     * @param schemaId Id of the schema in the database
     */
    void getSchemaByKey(Database.Collection collection, String schemaId, DatabaseListener listener);

    /**
     * Get a schema in a collection of the database
     * @param collection Collection in the database
     * @param queryMap Map of key / value (like where in SQL)
     */
    void getSchema(Database.Collection collection, Map<String, Object> queryMap, DatabaseListener listener);

    /**
     * Unload the database / remove all the listeners
     */
    void unload();
}
