package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.cnam.animalshealthbook.dialogs.ListSelectDialog;
import fr.cnam.animalshealthbook.models.holders.SchemaHolder;
import fr.cnam.animalshealthbook.models.schema.Schema;

/**
 * Dialog adapter to display list of items
 */
public class DialogListAdapter extends RecyclerView.Adapter<SchemaHolder>
{
    private List<? extends Schema> list;
    private ListSelectDialog.SelectItemListener listener;
    private Class<? extends SchemaHolder> holderClass;

    /**
     * Constructor
     * @param list List of schema
     * @param listener Listener when clicking on an item
     * @param holderClass Class holder of the item to display
     */
    public DialogListAdapter(List<? extends Schema> list,
                             ListSelectDialog.SelectItemListener listener,
                             Class<? extends SchemaHolder> holderClass)
    {
        this.list = list;
        this.listener = listener;
        this.holderClass = holderClass;
    }

    @NonNull
    @Override
    public SchemaHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        try{
            int layout = (int) holderClass.getMethod("getLayout").invoke(null);
            return holderClass.getDeclaredConstructor(View.class).newInstance(inflater.inflate(layout, parent, false));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull SchemaHolder schemaHolder, int pos)
    {
        Schema schema = list.get(pos);
        schemaHolder.setSchema(schema);
        schemaHolder.itemView.setOnClickListener(v -> listener.onItemSelected(schema, schema.getClass()));
    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    @Override
    public long getItemId(int position)
    {
        return list.get(position).hashCode();
    }
}
