package fr.cnam.animalshealthbook.models.database;

import fr.cnam.animalshealthbook.models.schema.Schema;

/**
 * Listen for events in a database
 */
public interface DatabaseListener
{
    /**
     * Triggered when adding or modifying a schema
     * @param schema The added or modified schema
     */
    void onGetSchema(Schema schema);

    /**
     * Triggered when removing a schema
     * @param schema The remove schema
     */
    void onRemoveSchema(Schema schema);
}
