package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.model.VaccineActivity;
import fr.cnam.animalshealthbook.models.holders.VaccineHolder;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.models.schema.Vaccines;

/**
 * Adapter to display vaccines in a list
 */
public class VaccinesAdapter extends RecyclerView.Adapter<VaccineHolder>
{
    private Vaccines vaccines;

    /**
     * Constructor
     * @param vaccines List of vaccines
     */
    public VaccinesAdapter(Vaccines vaccines)
    {
        this.vaccines = vaccines;
    }

    @NonNull
    @Override
    public VaccineHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new VaccineHolder(inflater.inflate(R.layout.vaccine_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VaccineHolder vaccineHolder, int pos)
    {
        Vaccine vaccine = (Vaccine) vaccines.values().toArray()[pos];

        vaccineHolder.setSchema(vaccine);

        vaccineHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(vaccineHolder.itemView.getContext(), VaccineActivity.class);
            intent.putExtra("vaccine", vaccine);

            vaccineHolder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return vaccines.size();
    }

    @Override
    public long getItemId(int pos)
    {
        return vaccines.values().toArray()[pos].hashCode();
    }
}
