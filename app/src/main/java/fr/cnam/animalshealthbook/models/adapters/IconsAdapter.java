package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.holders.IconHolder;
import fr.cnam.animalshealthbook.models.schema.Animals;

/**
 * Icons adapter to display icons
 */
public class IconsAdapter extends RecyclerView.Adapter<IconHolder>
{
    private IconHolder.IconHolderListener listener;

    /**
     * Constructor
     * @param listener when click on an icon trigger this listener
     */
    public IconsAdapter(IconHolder.IconHolderListener listener)
    {
        this.listener = listener;
    }

    @NonNull
    @Override
    public IconHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new IconHolder(inflater.inflate(R.layout.icon_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull IconHolder iconHolder, int pos)
    {
        iconHolder.setImages(listener, pos);
    }

    @Override
    public int getItemCount()
    {
        int length = Math.round(Animals.Ressources.size() / 3);
        return length * 3 < Animals.Ressources.size() ? length + 1 : length;
    }

    @Override
    public long getItemId(int position)
    {
        return Animals.Ressources.get(position);
    }
}
