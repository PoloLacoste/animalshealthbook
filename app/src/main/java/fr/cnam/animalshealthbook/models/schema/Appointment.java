package fr.cnam.animalshealthbook.models.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.models.database.Database;

/**
 * Model of Appointment
 */
public class Appointment extends Schema
{
    private String id;
    private Date date;
    private String veterinary;
    private String animal;

    /**
     * Constructor
     * @param id Database id
     * @param date date of the appointement
     * @param animal animal concerned
     * @param veterinary veterinary concerned
     */
    public Appointment(String id, Date date, String animal, String veterinary)
    {
        this(date, animal, veterinary);
        this.id = id;
    }
    /**
     * Constructor
     * @param date date of the appointement
     * @param animal animal concerned
     * @param veterinary veterinary concerned
     */
    public Appointment(Date date, String animal, String veterinary)
    {
        this.date = date;
        this.animal = animal;
        this.veterinary = veterinary;
    }

    @Override
    public String getId() {
        return id;
    }

    /**
     * Get the date of the appointment
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     * Get the veterinary
     * @return
     */
    public String getVeterinary() {
        return veterinary;
    }

    /**
     * get the animal
     * @return
     */
    public String getAnimal() {
        return animal;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public HashMap<String, Object> toMap()
    {
        return new HashMap<String, Object>(){{
            put("date", date.getTime());
            put("animal", animal);
            put("veterinary", veterinary);
        }};
    }

    @Override
    public void delete(Database db)
    {
        db.removeSchemaByKey(Database.Collection.APPOINTMENTS, id);
    }

    /**
     * Use it to transform map in appointment
     * @param map
     * @return
     */
    public static Schema fromMap(Map<String, Object> map)
    {
        return new Appointment(
                (String) map.get("id"),
                new Date((long) map.get("date")),
                (String) map.get("animal"),
                (String) map.get("veterinary")
        );
    }
}
