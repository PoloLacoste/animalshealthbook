package fr.cnam.animalshealthbook.models.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.models.database.Database;

/**
 * Model of intervention
 */
public class Intervention extends Schema
{
    private String id;
    private String name;
    private Date date;
    private String comment;
    private String animal;
    private Link link;
    private String idLink;

    /**
     * Constructor
     * @param id Database id
     * @param name name of intervention
     * @param date date of intervention
     * @param comment comment of intervention
     * @param animal animal concerned
     * @param link type of link
     * @param idLink database id of link
     */
    public Intervention(String id, String name, Date date, String comment, String animal,
                        Link link, String idLink)
    {
        this(name, date, comment, animal, link, idLink);
        this.id = id;
    }

    /**
     * Constructor
     * @param name name of intervention
     * @param date date of intervention
     * @param comment comment of intervention
     * @param animal animal concerned
     * @param link type of link
     * @param idLink database id of link
     */
    public Intervention(String name, Date date, String comment, String animal,
                        Link link, String idLink)
    {
        this.name = name;
        this.date = date;
        this.comment = comment;
        this.animal = animal;
        this.link = link;
        this.idLink = idLink;
    }

    /**
     * Enum of different type of link
     */
    public enum Link
    {
        VACCINE,
        DISEASE,
        EMPTY
    }

    /**
     * Get the Database id
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Get the name of intervention
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the date of intervention
     * @return
     */
    public Date getDate() {
        return date;
    }

    /**
     * Get the comment of the intervention
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     * Get the animal concerned
     * @return
     */
    public String getAnimal() { return animal; }

    /**
     * Get the type of link
     * @return
     */
    public Link getLink() {
        return link;
    }

    /**
     * Get the database id of link
     * @return
     */
    public String getIdLink() {
        return idLink;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public HashMap<String, Object> toMap()
    {
        return new HashMap<String, Object>() {{
            put("name", name);
            put("date", date.getTime());
            put("comment", comment);
            put("animal", animal);
            put("link", link.name());
            put("idLink", idLink);
        }};
    }

    @Override
    public void delete(Database db)
    {
        db.removeSchemaByKey(Database.Collection.INTERVENTIONS, id);
    }

    /**
     * Use it to convert a map into intervention
     * @param map
     * @return
     */
    public static Schema fromMap(Map<String, Object> map)
    {
        return new Intervention(
                (String)map.get("id"),
                (String)map.get("name"),
                new Date((long)map.get("date")),
                (String)map.get("comment"),
                (String)map.get("animal"),
                Link.valueOf((String)map.get("link")),
                (String)map.get("idLink"));
    }
}
