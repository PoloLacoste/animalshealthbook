package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.model.AppointmentActivity;
import fr.cnam.animalshealthbook.models.holders.AppointmentHolder;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Appointments;

/**
 * Adapter to display Appointments in a list
 */
public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentHolder>
{
    private Appointments appointments;

    /**
     * Constructor
     * @param appointments List of appointments
     */
    public AppointmentAdapter(Appointments appointments)
    {
        this.appointments = appointments;
    }

    @NonNull
    @Override
    public AppointmentHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new AppointmentHolder(inflater.inflate(R.layout.appointment_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AppointmentHolder appointmentHolder, int pos)
    {
        Appointment appointment = (Appointment) appointments.values().toArray()[pos];

        appointmentHolder.setSchema(appointment);

        appointmentHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(appointmentHolder.itemView.getContext(), AppointmentActivity.class);
            intent.putExtra("appointment", appointment);

            appointmentHolder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return appointments.size();
    }

    @Override
    public long getItemId(int pos)
    {
        return appointments.values().toArray()[pos].hashCode();
    }
}
