package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.model.DiseaseActivity;
import fr.cnam.animalshealthbook.models.holders.DiseaseHolder;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Diseases;

/**
 * Adapter to display diseases in a list
 */
public class DiseasesAdapter extends RecyclerView.Adapter<DiseaseHolder>
{
    private Diseases diseases;

    /**
     * Constructor
     * @param diseases List of Diseases
     */
    public DiseasesAdapter(Diseases diseases)
    {
        this.diseases = diseases;
    }

    @NonNull
    @Override
    public DiseaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        return new DiseaseHolder(inflater.inflate(R.layout.disease_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DiseaseHolder diseaseHolder, int pos)
    {
        Disease disease = (Disease) diseases.values().toArray()[pos];

        diseaseHolder.setSchema(disease);

        diseaseHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(diseaseHolder.itemView.getContext(), DiseaseActivity.class);
            intent.putExtra("disease", disease);

            diseaseHolder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return diseases.size();
    }

    @Override
    public long getItemId(int pos)
    {
        return diseases.values().toArray()[pos].hashCode();
    }
}
