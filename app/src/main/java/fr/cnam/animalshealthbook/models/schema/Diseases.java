package fr.cnam.animalshealthbook.models.schema;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import fr.cnam.animalshealthbook.R;

/**
 * List of Diseases
 */
public class Diseases extends HashMap<String, Disease>
{
    /**
     * List of picture
     */
    public static final List<Integer> Ressources = Arrays.asList(
            R.drawable.virus_1,
            R.drawable.virus_2,
            R.drawable.virus_3
    );

    /**
     * Get a random picture
     * @return
     */
    public static int randomRessource()
    {
        return Ressources.get(new Random().nextInt(Ressources.size()));
    }
}
