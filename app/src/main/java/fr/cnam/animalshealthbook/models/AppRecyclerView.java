package fr.cnam.animalshealthbook.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * A default app recycler view
 * we set options by default for optmisaation
 */
public class AppRecyclerView extends RecyclerView
{
    /**
     * Default contructor
     * @param context Context
     * @param attrs Attributes
     */
    public AppRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);

        setLayoutManager(new GridLayoutManager(context, 1));
        setHasFixedSize(true);
        setItemViewCacheSize(20);
    }
}
