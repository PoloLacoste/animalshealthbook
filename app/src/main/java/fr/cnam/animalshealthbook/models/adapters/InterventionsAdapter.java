package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.model.InterventionActivity;
import fr.cnam.animalshealthbook.models.holders.InterventionHolder;
import fr.cnam.animalshealthbook.models.schema.Intervention;
import fr.cnam.animalshealthbook.models.schema.Interventions;

/**
 * Adapter to display interventions in a list
 */
public class InterventionsAdapter extends RecyclerView.Adapter<InterventionHolder>
{
    private Interventions interventions;

    /**
     * Constructor
     * @param interventions List of interventions
     */
    public InterventionsAdapter(Interventions interventions)
    {
        this.interventions = interventions;
    }

    @NonNull
    @Override
    public InterventionHolder onCreateViewHolder(@NonNull ViewGroup parent, int pos)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new InterventionHolder(inflater.inflate(R.layout.intervention_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InterventionHolder interventionHolder, int pos)
    {
        Intervention intervention = (Intervention) interventions.values().toArray()[pos];

        interventionHolder.setSchema(intervention);

        interventionHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(interventionHolder.itemView.getContext(), InterventionActivity.class);
            intent.putExtra("intervention", intervention);
            interventionHolder.itemView.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return interventions.size();
    }

    @Override
    public long getItemId(int pos)
    {
        return interventions.values().toArray()[pos].hashCode();
    }
}
