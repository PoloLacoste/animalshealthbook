package fr.cnam.animalshealthbook.models.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.models.database.Database;

/**
 * Model of vaccine
 */
public class Vaccine extends Schema
{
    private String id;
    private String name;
    private Date dateOfTaking;
    private Date dateOfRenewale;
    private String comment;
    private String animal;

    /**
     * Constructor
     * @param id database id
     * @param name name of vaccine
     * @param dateOfTaking date of taking
     * @param dateOfRenewale date of renewale
     * @param comment comment
     * @param animal animal concerned
     */
    public Vaccine(String id, String name, Date dateOfTaking, Date dateOfRenewale, String comment, String animal)
    {
        this(name, dateOfTaking, dateOfRenewale, comment, animal);
        this.id = id;
    }
    /**
     * Constructor
     * @param name name of vaccine
     * @param dateOfTaking date of taking
     * @param dateOfRenewale date of renewale
     * @param comment comment
     * @param animal animal concerned
     */
    public Vaccine(String name, Date dateOfTaking, Date dateOfRenewale, String comment, String animal)
    {
        this.name = name;
        this.dateOfTaking = dateOfTaking;
        this.dateOfRenewale = dateOfRenewale;
        this.comment = comment;
        this.animal = animal;
    }

    /*      Getters and Setters   */

    /**
     * Get the database id
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Get the name of vaccin
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * Get the date of taking
     * @return
     */
    public Date getDateOfTaking() {
        return dateOfTaking;
    }

    /**
     * Get the comment
     * @return
     */
    public String getComment() {
        return comment;
    }

    /**
     * Get the animal concerned
     * @return
     */
    public String getAnimal() {
        return animal;
    }

    /**
     * Get the date of renewale
     * @return
     */
    public Date getDateOfRenewale() {
        return dateOfRenewale;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public HashMap<String, Object> toMap()
    {
        return new HashMap<String, Object>() {{
            put("name", name);
            put("dateOfTaking", dateOfTaking.getTime());
            put("dateOfRenewale", dateOfRenewale == null ? -1 : dateOfRenewale.getTime());
            put("comment", comment);
            put("animal", animal);
        }};
    }

    @Override
    public void delete(Database db)
    {
        db.removeSchema(Database.Collection.INTERVENTIONS, new HashMap<String, Object>(){{
            put("idLink", id);
        }});

        db.removeSchemaByKey(Database.Collection.VACCINES, id);
    }

    /**
     * Use it to convert a map into vaccine
     * @param map
     * @return
     */
    public static Schema fromMap(Map<String, Object> map)
    {
        long renewale = (long)map.get("dateOfRenewale");
        Date date_renewale = renewale == -1 ? null : new Date(renewale);

        return new Vaccine((String)map.get("id"), (String)map.get("name"),
                new Date((long)map.get("dateOfTaking")), date_renewale, (String)map.get("comment"),
                (String)map.get("animal"));
    }
}
