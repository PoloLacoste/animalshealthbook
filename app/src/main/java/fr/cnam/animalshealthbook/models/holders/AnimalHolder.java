package fr.cnam.animalshealthbook.models.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Schema;

/**
 * Holder for display one animal
 */
public class AnimalHolder extends SchemaHolder
{
    private Animal animal;

    private TextView name, animalId;
    private ImageView animalPicture, sexImage;

    private Context context;

    /**
     * constructor of the holder
     * @param view
     */
    public AnimalHolder(@NonNull View view)
    {
        super(view);

        name = view.findViewById(R.id.animalName);
        animalPicture = view.findViewById(R.id.animalPicture);
        animalId = view.findViewById(R.id.animalId);
        sexImage = view.findViewById(R.id.animalSex);

        context = view.getContext();
    }

    @Override
    public void setSchema(Schema schema)
    {
        animal = (Animal) schema;

        name.setText(animal.getName());
        animalId.setText(animal.getNumId());

        animal.setPicture(context, animalPicture);
        sexImage.setImageResource(animal.getSexRessource());
    }

    /**
     * Use it for get the layout
     * @return int
     */
    public static int getLayout()
    {
        return R.layout.animal_list_item;
    }

    /**
     * Recycle the image
     */
    public void recycle()
    {
        Glide.with(context).clear(animalPicture);
    }
}
