package fr.cnam.animalshealthbook.models.database;

import fr.cnam.animalshealthbook.models.schema.Schema;

/**
 * Interface listening only for event when adding or removing schema
 */
public interface SimpleDatabaseListener
{
    void onGetSchema(Schema schema);
}
