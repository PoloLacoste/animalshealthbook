package fr.cnam.animalshealthbook.models.holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.Arrays;
import java.util.List;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Animals;

/**
 * Icon vue in a list
 */
public class IconHolder extends RecyclerView.ViewHolder
{
    private List<Integer> imageViews = Arrays.asList(R.id.first, R.id.second, R.id.third);;

    /**
     * When clicking on the icon trigger this method
     */
    public interface IconHolderListener
    {
        /**
         * @param res The id of the icon (int)
         */
        void onSelectIcon(int res);
    }

    public IconHolder(@NonNull View itemView)
    {
        super(itemView);
    }

    /**
     * Set image in a row
     * @param listener Listener triggered when clicking on an icon
     * @param id id of the row
     */
    public void setImages(IconHolderListener listener, int id)
    {
        // on each row we have 3 icons
        // recovering the start of the list
        int start = id == 0 ? id : id * 3;

        // make sure we are ont outside of the list of icons
        int end = start + 3 > Animals.Ressources.size() ? Animals.Ressources.size() : start + 3;

        // list of icons
        List<Integer> ressoucesId = Animals.Ressources.subList(start, end);

        for(int i = 0; i < ressoucesId.size(); i++)
        {
            final int ressource = ressoucesId.get(i);

            ImageView imageView = itemView.findViewById(imageViews.get(i));
            imageView.setImageResource(ressource);
            imageView.setOnClickListener(v -> listener.onSelectIcon(ressource));
        }
    }
}
