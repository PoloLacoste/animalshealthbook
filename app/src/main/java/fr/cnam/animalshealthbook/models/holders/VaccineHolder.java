package fr.cnam.animalshealthbook.models.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.models.schema.Vaccines;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Holder for display one Vaccine
 */
public class VaccineHolder extends SchemaHolder
{
    private ImageView imageView;
    private Vaccine vaccine;
    private TextView name;
    private TextView dateOfTaking;

    private Context context;

    /**
     * constructor of the holder
     * @param view
     */
    public VaccineHolder(@NonNull View view)
    {
        super(view);

        imageView = view.findViewById(R.id.vaccineImage);
        name = view.findViewById(R.id.vaccineName);
        dateOfTaking = view.findViewById(R.id.dateOfTaking);
        context = view.getContext();
    }

    @Override
    public void setSchema(Schema schema) {
        vaccine = (Vaccine)schema;

        imageView.setImageResource(Vaccines.randomRessource());
        name.setText(vaccine.getName());
        dateOfTaking.setText(String.format("%s %s", context.getString(R.string.taken_on),
                Utils.dateFormat(vaccine.getDateOfTaking())));
    }

    /**
     * Use it for get the layout
     * @return
     */
    public static int getLayout()
    {
        return R.layout.vaccine_list_item;
    }
}
