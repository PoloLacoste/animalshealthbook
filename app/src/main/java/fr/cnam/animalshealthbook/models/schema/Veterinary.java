package fr.cnam.animalshealthbook.models.schema;

import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.models.database.Database;

/**
 * Model of veterinary
 */
public class Veterinary extends Schema
{
    private String id;
    private String name;
    private String address;
    private String phone;
    private String mail;
    private String animal;

    /**
     * Constructeur
     * @param id database id
     * @param name name of veterinary
     * @param address adresse of veterinary
     * @param phone phone number
     * @param mail email
     * @param animal anial concerned
     */
    public Veterinary(String id, String name, String address, String phone,
                      String mail, String animal)
    {
        this(name, address, phone, mail, animal);
        this.id = id;
    }
    /**     * Constructeur
    * @param name name of veterinary
    * @param address adresse of veterinary
    * @param phone phone number
    * @param mail email
    * @param animal anial concerned
    */
    public Veterinary(String name, String address, String phone, String mail, String animal)
    {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.mail = mail;
        this.animal = animal;
    }

    /**
     * Get the database id
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Get the name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the address
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     * Get the phone number
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Get the email
     * @return
     */
    public String getMail() {
        return mail;
    }

    /**
     * Get the animal concerned
     * @return
     */
    public String getAnimal() {
        return animal;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Set animal concerned
     * @param animal
     */
    public void setAnimal(String animal) {
        this.animal = animal;
    }

    @Override
    public HashMap<String, Object> toMap()
    {
        return new HashMap<String, Object>(){{
           put("name", name);
           put("address", address);
           put("phone", phone);
           put("mail", mail);
           put("animal", animal);
        }};
    }

    @Override
    public void delete(Database db)
    {
        db.removeSchema(Database.Collection.APPOINTMENTS, new HashMap<String, Object>(){{
            put("veterinary", id);
        }});

        db.removeSchemaByKey(Database.Collection.VETERINARIANS, id);
    }

    /**
     * Use it to convert map into veterinary
     * @param map
     * @return
     */
    public static Veterinary fromMap(Map<String, Object> map)
    {
        return new Veterinary((String) map.get("id"), (String) map.get("name"),
                (String) map.get("address"), (String) map.get("phone"),
                (String) map.get("mail"), (String) map.get("animal"));
    }
}
