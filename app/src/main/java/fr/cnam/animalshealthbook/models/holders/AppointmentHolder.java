package fr.cnam.animalshealthbook.models.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Holder for display one appointment
 */
public class AppointmentHolder extends SchemaHolder
{
    private Appointment appointment;

    private TextView date;
    private TextView info;

    private Context context;
    private Database db;

    /**
     * constructor of the holder
     * @param view
     */
    public AppointmentHolder(@NonNull View view)
    {
        super(view);

        date = view.findViewById(R.id.date);
        info = view.findViewById(R.id.info);

        context = view.getContext();
        db = new Database();
    }

    @Override
    public void setSchema(Schema schema)
    {
        appointment = (Appointment) schema;

        date.setText(Utils.dateFormat(appointment.getDate()));

        db.getSchemaByKey(Database.Collection.VETERINARIANS, appointment.getVeterinary(),
                new DatabaseListener() {
                    @Override
                    public void onGetSchema(Schema schema)
                    {
                        Veterinary veterinary = (Veterinary) schema;
                        info.setText(String.format("%s %s - %s", context.getString(R.string.with),
                                veterinary.getName(), veterinary.getAddress()));
                    }

                    @Override
                    public void onRemoveSchema(Schema schema) { }
                });
    }

    /**
     * Use it for get the layout
     * @return
     */
    public static int getLayout()
    {
        return R.layout.appointment_list_item;
    }
}
