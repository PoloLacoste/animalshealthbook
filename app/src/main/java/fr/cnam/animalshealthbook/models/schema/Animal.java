package fr.cnam.animalshealthbook.models.schema;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Model of Animal
 */
public class Animal extends Schema
{
    private String id;
    private String numId;
    private String name;
    private Date dob;
    private String file;
    private int res = R.drawable.animal_default_picture;
    private String species;
    private String race;
    private Sex sex;

    /**
     * Constructor
     * @param numId identification number
     * @param name name
     * @param dob date of birth
     * @param res id of the select img
     * @param species species
     * @param race race
     * @param sex sex(enum)
     */
    public Animal(String numId, String name, Date dob, int res, String species, String race, Sex sex)
    {
        this(null, numId, name, dob, res, species, race, sex);
        this.res = res;
    }
    /**
     * Constructor
     * @param numId identification number
     * @param name name
     * @param dob date of birth
     * @param species species
     * @param race race
     * @param sex sex(enum)
     */
    public Animal(String numId, String name, Date dob, String file, String species, String race, Sex sex)
    {
        this(null, numId, name, dob, file, species, race, sex);
		this.file = file;
    }
    /**
     * Constructor
     * @param id id of database
     * @param numId identification number
     * @param name name
     * @param dob date of birth
     * @param species species
     * @param race race
     * @param sex sex(enum)
     */
    public Animal(String id, String numId, String name, Date dob, String file, String species, String race, Sex sex)
    {
        this(id, numId, name, dob, species, race, sex);
        this.file = file;
    }
    /**
     * Constructor
     * @param id id of database
     * @param numId identification number
     * @param name name
     * @param dob date of birth
     * @param res id of the select img
     * @param species species
     * @param race race
     * @param sex sex(enum)
     */
    public Animal(String id, String numId, String name, Date dob, int res, String species, String race, Sex sex)
    {
        this(id, numId, name, dob, species, race, sex);
        this.res = res;
    }

    /**
     * Constructor
     * @param id id of database
     * @param numId identification number
     * @param name name
     * @param dob date of birth
     * @param species species
     * @param race race
     * @param sex sex(enum)
     */
    private Animal(String id, String numId, String name, Date dob, String species, String race, Sex sex)
    {
        this.id = id;
        this.numId = numId;
        this.name = name;
        this.dob = dob;
        this.species = species;
        this.race = race;
        this.sex = sex;
    }

    /**
     * Enum of different sex for an animal
     */
    public enum Sex
    {
        MALE,
        FEMALE,
        UNKNOWN
    }

    @Override
    public String getId() {
        return id;
    }

    /**
     * Get the identification number
     * @return
     */
    public String getNumId() {
        return numId;
    }

    /**
     * Get the name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get the date of birth
     * @return
     */
    public Date getDob()
    {
        return dob;
    }

    /**
     * Set the image picture
     * @param context
     * @param imageView
     */
    public void setPicture(Context context, ImageView imageView)
    {
        if(Utils.isEmpty(file))
        {
            imageView.setImageResource(res);
        }
        else
        {
            Glide.with(context)
                    .asBitmap()
                    .load(file)
                    .thumbnail(0.5f)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.animal_default_picture)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        }
    }

    /**
     * Get the species
     * @return
     */
    public String getSpecies() {
        return species;
    }

    /**
     * get the race
     * @return
     */
    public String getRace() {
        return race;
    }

    /**
     * get the sex
     * @return
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Get the picture who correspond to the sex
     * @return
     */
    public int getSexRessource()
    {
        switch (sex)
        {
            case MALE:
                return R.drawable.male;
            case FEMALE:
                return R.drawable.female;
            default:
                return R.drawable.unknown;
        }
    }

    /**
     * Get the int icone
     * @return
     */
    public int getRes() {
        return res;
    }

    /**
     * Get the file of the picture
     * @return
     */
    public String getFile() {
        return file;
    }

    @Override
    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public HashMap<String, Object> toMap()
    {
        return new HashMap<String, Object>() {{
            put("name", name);
            put("numId", numId);
            put("dob", dob.getTime());
            put("file", file);
            put("res", res);
            put("species", species);
            put("race", race);
            put("sex", sex.name());
        }};
    }

    @Override
    public void delete(Database db)
    {
        db.removeSchema(Database.Collection.VACCINES, new HashMap<String, Object>(){{
            put("animal", id);
        }});

        db.removeSchema(Database.Collection.DISEASES, new HashMap<String, Object>(){{
            put("animal", id);
        }});

        db.removeSchema(Database.Collection.INTERVENTIONS, new HashMap<String, Object>(){{
            put("animal", id);
        }});

        db.removeSchema(Database.Collection.APPOINTMENTS, new HashMap<String, Object>(){{
            put("animal", id);
        }});

        db.getSchema(Database.Collection.VETERINARIANS, new HashMap<String, Object>() {{
            put("animal", id);
        }}, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                Veterinary veterinary = (Veterinary) schema;
                veterinary.setAnimal(null);

                db.updateSchema(Database.Collection.VETERINARIANS, veterinary);
            }

            @Override
            public void onRemoveSchema(Schema schema) { }
        });

        db.removeSchemaByKey(Database.Collection.ANIMALS, id);
    }

    /**
     * Use it to transform a map in animal
     * @param map
     * @return
     */
    public static Schema fromMap(Map<String, Object> map)
    {
        String file = (String) map.get("file");

        if(TextUtils.isEmpty(file))
        {
            return new Animal(
                    (String)map.get("id"),
                    (String)map.get("numId"),
                    (String)map.get("name"),
                    new Date((long) map.get("dob")),
                    (int)(long)map.get("res"),
                    (String)map.get("species"),
                    (String) map.get("race"),
                    Sex.valueOf((String)map.get("sex"))
            );
        }
        else
        {
            return new Animal(
                    (String)map.get("id"),
                    (String)map.get("numId"),
                    (String)map.get("name"),
                    new Date((long)map.get("dob")),
                    file, (String)map.get("species"),
                    (String) map.get("race"),
                    Sex.valueOf((String)map.get("sex"))
            );
        }
    }
}

