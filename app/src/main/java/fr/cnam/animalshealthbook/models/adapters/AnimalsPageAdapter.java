package fr.cnam.animalshealthbook.models.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Page Adapter for AnimalsActivity
 */
public class AnimalsPageAdapter extends FragmentPagerAdapter
{
    private Context context;
    private List<Fragment> fragments;

    /**
     * Default constructor with a list of fragments
     * @param context Context
     * @param fm FragmentManager
     * @param fragments List of fragments to display
     */
    public AnimalsPageAdapter(Context context, FragmentManager fm, List<Fragment> fragments)
    {
        super(fm);
        this.context = context;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int i)
    {
        return fragments.get(i);
    }

    @Override
    public int getCount()
    {
        return fragments.size();
    }
}
