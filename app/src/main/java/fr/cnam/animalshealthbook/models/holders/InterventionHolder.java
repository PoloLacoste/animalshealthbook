package fr.cnam.animalshealthbook.models.holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.models.schema.Intervention;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Holder for display one intervention
 */
public class InterventionHolder extends SchemaHolder
{
    private Intervention intervention;
    private TextView name;
    private TextView date;
    private Context context;
    private ImageView icon;

    /**
     * constructor of the holder
     * @param itemView
     */
    public InterventionHolder(@NonNull View itemView)
    {
        super(itemView);

        icon = itemView.findViewById(R.id.icon);
        name = itemView.findViewById(R.id.name);
        date = itemView.findViewById(R.id.date);

        context = itemView.getContext();
    }

    @Override
    public void setSchema(Schema schema)
    {
        intervention= (Intervention) schema;

        name.setText(intervention.getName());
        date.setText(Utils.dateFormat(intervention.getDate()));

        switch (intervention.getLink())
        {
            case VACCINE: icon.setImageResource(R.drawable.vaccine_1); break;
            case DISEASE: icon.setImageResource(R.drawable.virus_1); break;
        }
    }

    /**
     * Use it for get the layout
     * @return
     */
    public static int getLayout()
    {
        return R.layout.intervention_list_item;
    }
}