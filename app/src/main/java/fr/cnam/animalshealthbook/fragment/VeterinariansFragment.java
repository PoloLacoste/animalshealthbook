package fr.cnam.animalshealthbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddVeterinaryActivity;
import fr.cnam.animalshealthbook.activity.AnimalsActivity;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.VeterinaryAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinarians;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Fragment for display a list of Veterinarians
 */
public class VeterinariansFragment extends Fragment
{
    private Veterinarians veterinarians;

    private VeterinaryAdapter adapter;
    private AppRecyclerView recyclerView;

    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        veterinarians = new Veterinarians();
        adapter = new VeterinaryAdapter(veterinarians);
        adapter.setHasStableIds(true);

        getDb().getSchema(Database.Collection.VETERINARIANS, null, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                veterinarians.put(schema.getId(), (Veterinary) schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                veterinarians.remove(schema.getId());
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_veterinarians, container, false);
        }

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.addVeterinary).setOnClickListener(v -> goToAdd());

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_VETERINARY:
                if (data != null)
                {
                    getDb().addSchema(Database.Collection.VETERINARIANS,
                            (Schema) data.getSerializableExtra("veterinary"), null);
                }
                break;
        }
    }

    private void goToAdd()
    {
        Intent intent = new Intent(getContext(), AddVeterinaryActivity.class);
        startActivityForResult(intent, Utils.ADD_VETERINARY);
    }

    private Database getDb()
    {
        return ((AnimalsActivity) getActivity()).getDb();
    }
}
