package fr.cnam.animalshealthbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.HashMap;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddAnimalActivity;
import fr.cnam.animalshealthbook.activity.model.AnimalActivity;
import fr.cnam.animalshealthbook.activity.model.AppointmentActivity;
import fr.cnam.animalshealthbook.activity.model.VeterinaryActivity;
import fr.cnam.animalshealthbook.dialogs.ConfirmationDialog;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Veterinary;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Fragment who display information about animal
 */
public class AnimalFragment extends Fragment
{
    private Animal animal;
    private Veterinary veterinary;
    private Appointment appointment;

    private ImageView animalPicture;
    private ImageView sexPicture;
    private TextView idNum;
    private TextView dob;
    private TextView species, race;

    private TextView veterinaryName;
    private TextView veterinaryPhone;

    private View cardAppointment;
    private TextView titleNextAppointment;
    private TextView nextAppointment;

    private View view;

    private ConfirmationDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.animal = ((AnimalActivity) getActivity()).getAnimal();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_animal, container, false);
        }

        animalPicture = view.findViewById(R.id.animalPicture);
        sexPicture = view.findViewById(R.id.sexPicture);
        idNum = view.findViewById(R.id.id);
        dob = view.findViewById(R.id.dob);

        species = view.findViewById(R.id.species);
        race = view.findViewById(R.id.race);
        veterinaryName = view.findViewById(R.id.veterinayName);
        veterinaryPhone = view.findViewById(R.id.phone);

        cardAppointment = view.findViewById(R.id.card_rdv);
        titleNextAppointment = view.findViewById(R.id.title_next_appointment);
        nextAppointment = view.findViewById(R.id.next_appointment);

        veterinaryName.setText(getString(R.string.no_veterinary));
        veterinaryName.setTextSize(getResources().getDimension(R.dimen.card_title) /
                getResources().getDisplayMetrics().density);
        veterinaryPhone.setVisibility(View.INVISIBLE);

        setAnimalView();
        getVeterinary();

        setHasOptionsMenu(true);

        return view;
    }

    private void setAnimalView()
    {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(animal.getName());

        animal.setPicture(getContext(), animalPicture);

        sexPicture.setImageResource(animal.getSexRessource());
        idNum.setText(animal.getNumId());
        dob.setText(Utils.dateFormat(animal.getDob()));


        species.setText(animal.getSpecies());
        race.setText(animal.getRace());
    }

    private void getVeterinary()
    {
        getDb().getSchema(Database.Collection.VETERINARIANS, new HashMap<String, Object>() {{
            put("animal", animal.getId());
        }}, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                veterinary = (Veterinary) schema;

                veterinaryName.setText(veterinary.getName());
                veterinaryPhone.setVisibility(View.VISIBLE);
                veterinaryPhone.setText(veterinary.getPhone());

                getView().findViewById(R.id.card_veterinary).setOnClickListener(v -> goToVeterinary());
                getAppointment();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                if(schema.getId().equals(veterinary.getId()))
                {
                    veterinaryName.setText(getString(R.string.no_veterinary));
                    veterinaryPhone.setVisibility(View.INVISIBLE);
                    veterinaryPhone.setText("");
                    getVeterinary();
                }
            }
        });
    }

    private void getAppointment()
    {
        getDb().getSchema(Database.Collection.APPOINTMENTS, new HashMap<String, Object>() {{
            put("animal", animal.getId());
            put("veterinary", veterinary.getId());
        }}, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                Appointment temp = (Appointment) schema;

                if(appointment == null)
                {
                    appointment = temp;
                    setAppointmentView();
                }
                else if(temp.getDate().after(new Date()) && temp.getDate().before(appointment.getDate()))
                {
                    appointment = temp;
                    setAppointmentView();
                }
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                if(schema.getId().equals(appointment.getId()))
                {
                    appointment = null;
                    titleNextAppointment.setText(getString(R.string.no_appointment));
                    nextAppointment.setText("");
                    getAppointment();
                }
            }
        });
    }

    private void setAppointmentView()
    {
        cardAppointment.setOnClickListener(v -> goToAppointment());

        String msg = appointment.getDate().before(new Date()) ?
                getString(R.string.last_appointment) : getString(R.string.next_appointment);
        titleNextAppointment.setText(msg);

        nextAppointment.setText(Utils.dateFormat(appointment.getDate()));
    }

    private void goToAppointment()
    {
        Intent intent = new Intent(getContext(), AppointmentActivity.class);
        intent.putExtra("appointment", appointment);
        startActivity(intent);
    }

    private void goToVeterinary()
    {
        Intent intent = new Intent(getContext(), VeterinaryActivity.class);
        intent.putExtra("veterinary", veterinary);
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.animal_toolbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.edit: editAnimal(); return true;
            case R.id.delete: confirmationDelete(getString(R.string.confirm_deletion),
                    getString(R.string.sure_delete_animal));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmationDelete(String title, String description)
    {
        dialog = ConfirmationDialog.newInstance(title, description);
        dialog.show(getActivity().getSupportFragmentManager(), "ConfirmationDialog");
    }

    private void editAnimal()
    {
        Intent intent = new Intent(getContext(), AddAnimalActivity.class);
        intent.putExtra("animal", animal);
        startActivityForResult(intent, Utils.MODIFY_ANIMAL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.MODIFY_ANIMAL:
                if(data != null)
                {
                    Schema schema = (Schema) data.getSerializableExtra("animal");
                    schema.setId(animal.getId());

                    getDb().updateSchema(Database.Collection.ANIMALS, schema);

                    animal = (Animal) schema;
                    setAnimalView();
                }
                break;
        }
    }

    /**
     * Function for delete the animal
     */
    public void deleteAnimal()
    {
        dialog.dismiss();
        animal.delete(getDb());
        getActivity().finish();
    }

    private Database getDb()
    {
        return ((AnimalActivity) getActivity()).getDb();
    }
}
