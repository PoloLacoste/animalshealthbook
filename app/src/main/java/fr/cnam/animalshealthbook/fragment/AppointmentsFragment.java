package fr.cnam.animalshealthbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddAppointmentActivity;
import fr.cnam.animalshealthbook.activity.AnimalsActivity;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.AppointmentAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Appointment;
import fr.cnam.animalshealthbook.models.schema.Appointments;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Fragment for display a list of appointments
 */
public class AppointmentsFragment extends Fragment
{
    private AppRecyclerView recyclerView;
    private AppointmentAdapter adapter;
    private Appointments appointments;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        appointments = new Appointments();
        adapter = new AppointmentAdapter(appointments);
        adapter.setHasStableIds(true);

        getDb().getSchema(Database.Collection.APPOINTMENTS, null, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                appointments.put(schema.getId(), (Appointment) schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                appointments.remove(schema.getId());
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_appointments, container, false);
        }

        recyclerView = view.findViewById(R.id.appointmentList);

        view.findViewById(R.id.addAppointment).setOnClickListener(v -> goToAdd());

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_APPOINTMENT:
                if (data != null)
                {
                    getDb().addSchema(Database.Collection.APPOINTMENTS,
                            (Schema) data.getSerializableExtra("appointment"), null);
                }
                break;
        }
    }

    private void goToAdd()
    {
        Intent intent = new Intent(getActivity(), AddAppointmentActivity.class);
        startActivityForResult(intent, Utils.ADD_APPOINTMENT);
    }

    private Database getDb()
    {
        return ((AnimalsActivity) getActivity()).getDb();
    }
}
