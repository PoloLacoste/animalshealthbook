package fr.cnam.animalshealthbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddAnimalActivity;
import fr.cnam.animalshealthbook.activity.AnimalsActivity;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.AnimalsAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Animals;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Fragment who display a list of animals
 */
public class AnimalsFragment extends Fragment
{
    private AppRecyclerView recyclerView;
    private AnimalsAdapter adapter;
    private Animals animals;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        animals = new Animals();
        adapter = new AnimalsAdapter(animals);
        adapter.setHasStableIds(true);

        getDb().getSchema(Database.Collection.ANIMALS, null, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                animals.put(schema.getId(), (Animal) schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                animals.remove(schema.getId());
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_animals, container, false);
        }

        recyclerView = view.findViewById(R.id.animalsList);
        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.addAnimal).setOnClickListener(v -> goToAdd());

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_ANIMAL:
                if (data != null)
                {
                    getDb().addSchema(Database.Collection.ANIMALS,
                            (Schema) data.getSerializableExtra("animal"), null);
                }
                break;
        }
    }

    private void goToAdd()
    {
        Intent intent = new Intent(getContext(), AddAnimalActivity.class);
        startActivityForResult(intent, Utils.ADD_ANIMAL);
    }

    private Database getDb()
    {
        return ((AnimalsActivity) getActivity()).getDb();
    }
}
