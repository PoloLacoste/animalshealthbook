package fr.cnam.animalshealthbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddVaccineActivity;
import fr.cnam.animalshealthbook.activity.model.AnimalActivity;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.VaccinesAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.models.schema.Vaccine;
import fr.cnam.animalshealthbook.models.schema.Vaccines;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Fragment for display a list of Vaccines
 */
public class VaccinesFragment extends Fragment
{
    private VaccinesAdapter adapter;
    private AppRecyclerView recyclerView;

    private Vaccines vaccines;

    private Animal animal;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.animal = ((AnimalActivity) getActivity()).getAnimal();

        vaccines = new Vaccines();
        adapter = new VaccinesAdapter(vaccines);
        adapter.setHasStableIds(true);

        getDb().getSchema(Database.Collection.VACCINES, new HashMap<String, Object>() {{
            put("animal", animal.getId());
        }}, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                vaccines.put(schema.getId(), (Vaccine) schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                vaccines.remove(schema.getId());
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_vaccines, container, false);
        }

        recyclerView = view.findViewById(R.id.recyclerView);

        view.findViewById(R.id.addVaccine).setOnClickListener(v -> goToAdd());

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_VACCINE:
                if (data != null)
                {
                    getDb().addSchema(Database.Collection.VACCINES,
                            (Schema) data.getSerializableExtra("vaccine"), null);
                }
                break;
        }
    }

    private void goToAdd()
    {
        Intent intent = new Intent(getActivity(), AddVaccineActivity.class);
        intent.putExtra("animal", animal.getId());
        startActivityForResult(intent, Utils.ADD_VACCINE);
    }

    private Database getDb()
    {
        return ((AnimalActivity) getActivity()).getDb();
    }
}
