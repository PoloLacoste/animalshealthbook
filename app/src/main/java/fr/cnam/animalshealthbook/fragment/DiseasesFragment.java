package fr.cnam.animalshealthbook.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddDiseaseActivity;
import fr.cnam.animalshealthbook.activity.model.AnimalActivity;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.DiseasesAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Disease;
import fr.cnam.animalshealthbook.models.schema.Diseases;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;

/**
 * Fragment for display a list of Diseases
 */
public class DiseasesFragment extends Fragment
{
    private AppRecyclerView recyclerView;
    private DiseasesAdapter adapter;
    private Diseases diseases;

    private Animal animal;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.animal = ((AnimalActivity) getActivity()).getAnimal();

        diseases = new Diseases();
        adapter = new DiseasesAdapter(diseases);
        adapter.setHasStableIds(true);

        getDb().getSchema(Database.Collection.DISEASES, new HashMap<String, Object>() {{
            put("animal", animal.getId());
        }}, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                diseases.put(schema.getId(), (Disease) schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                diseases.remove(schema.getId());
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_diseases, container, false);
        }

        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.addDisease).setOnClickListener(v -> goToAdd());

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_DISEASE:
                if (data != null)
                {
                    getDb().addSchema(Database.Collection.DISEASES,
                            (Schema) data.getSerializableExtra("disease"), null);
                }
                break;
        }
    }

    private void goToAdd()
    {
        Intent intent = new Intent(getActivity(), AddDiseaseActivity.class);
        intent.putExtra("animal", animal.getId());
        startActivityForResult(intent, Utils.ADD_DISEASE);
    }

    private Database getDb()
    {
        return ((AnimalActivity) getActivity()).getDb();
    }
}
