package fr.cnam.animalshealthbook.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import fr.cnam.animalshealthbook.R;
import fr.cnam.animalshealthbook.activity.AddInterventionActivity;
import fr.cnam.animalshealthbook.activity.model.AnimalActivity;
import fr.cnam.animalshealthbook.models.AppRecyclerView;
import fr.cnam.animalshealthbook.models.adapters.InterventionsAdapter;
import fr.cnam.animalshealthbook.models.database.Database;
import fr.cnam.animalshealthbook.models.database.DatabaseListener;
import fr.cnam.animalshealthbook.models.schema.Animal;
import fr.cnam.animalshealthbook.models.schema.Intervention;
import fr.cnam.animalshealthbook.models.schema.Interventions;
import fr.cnam.animalshealthbook.models.schema.Schema;
import fr.cnam.animalshealthbook.utils.Utils;
/**
 * Fragment for display a list of Interventions
 */
public class InterventionsFragment extends Fragment
{
    private AppRecyclerView recyclerView;
    private InterventionsAdapter adapter;

    private Interventions interventions;

    private Animal animal;

    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.animal = ((AnimalActivity) getActivity()).getAnimal();

        interventions = new Interventions();
        adapter = new InterventionsAdapter(interventions);
        adapter.setHasStableIds(true);

        getDb().getSchema(Database.Collection.INTERVENTIONS, new HashMap<String, Object>() {{
            put("animal", animal.getId());
        }}, new DatabaseListener() {
            @Override
            public void onGetSchema(Schema schema)
            {
                interventions.put(schema.getId(), (Intervention) schema);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onRemoveSchema(Schema schema)
            {
                interventions.remove(schema.getId());
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(view == null)
        {
            view = inflater.inflate(R.layout.fragment_interventions, container, false);
        }

        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.addIntervention).setOnClickListener(v -> goToAdd());

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case Utils.ADD_INTERVENTION:
                if (data != null)
                {
                    getDb().addSchema(Database.Collection.INTERVENTIONS,
                            (Schema) data.getSerializableExtra("intervention"), null);
                }
                break;
        }
    }

    private void goToAdd()
    {
        Intent intent = new Intent(getActivity(), AddInterventionActivity.class);
        intent.putExtra("animal", animal.getId());
        startActivityForResult(intent, Utils.ADD_INTERVENTION);
    }

    private Database getDb()
    {
        return ((AnimalActivity) getActivity()).getDb();
    }
}
