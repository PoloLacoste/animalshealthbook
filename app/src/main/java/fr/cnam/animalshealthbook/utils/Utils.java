package fr.cnam.animalshealthbook.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils
{
    public static final String TAG = "AnimalsHealthBook";

    public static final int REQUEST_PERMISSIONS = 1;

    public static final int OPEN_GALLERY = 10;
    public static final int TAKE_PICTURE = 11;

    public static final int ADD_ANIMAL = 110;
    public static final int ADD_DISEASE = 111;
    public static final int ADD_VACCINE = 112;
    public static final int ADD_VETERINARY = 113;
    public static final int ADD_INTERVENTION = 114;
    public static final int ADD_APPOINTMENT = 115;

    public static final int MODIFY_ANIMAL = 120;
    public static final int MODIFY_DISEASE = 121;
    public static final int MODIFY_VACCINE = 122;
    public static final int MODIFY_VETERINARY = 123;
    public static final int MODIFY_INTERVENTION = 124;
    public static final int MODIFY_APPOINTMENT = 125;

    /**
     * Transform an uri to file path
     * @param context Context
     * @param uri Uri
     * @return the file path
     */
    public static String getRealPathBelowVersion(Context context, Uri uri)
    {
        String result;
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null)
        {
            result = uri.getPath();
        }
        else
        {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    /**
     * Transform a calendar to string
     * @param calendar Calendar
     * @return String
     */
    public static String dateFormat(Calendar calendar)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        sdf.setTimeZone(calendar.getTimeZone());
        return sdf.format(calendar.getTime());
    }

    /**
     * Transform a date to string
     * @param date Date
     * @return String
     */
    public static String dateFormat(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return dateFormat(calendar);
    }

    /**
     * Check if a string is empty
     * @param str String
     * @return boolean
     */
    public static boolean isEmpty(String str)
    {
        if(str == null)
        {
            return true;
        }
        return str.replace(" ", "").isEmpty();
    }

    /**
     * Create a random image file name in the application directory
     * @param context Context
     * @return File
     */
    public static File createImageFile(Context context)
    {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        try{
            return File.createTempFile(imageFileName, ".jpg", storageDir);
        }
        catch (IOException e) {}

        return null;
    }

}
