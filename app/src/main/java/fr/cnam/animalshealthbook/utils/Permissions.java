package fr.cnam.animalshealthbook.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.cnam.animalshealthbook.models.Permission;

/**
 * Hashmap of permission with permission name as key
 */
public class Permissions extends HashMap<String, Permission>
{
    /**
     * Add permission in the permissions Hashmap
     * @param perms List of permissions name
     */
    public void addPermissions(String[] perms)
    {
        for(String perm : perms)
        {
            put(perm, new Permission(perm));
        }
    }

    /**
     * Check ithe permission is granted
     * @param context Context of the app
     * @param name Permission name
     * @return if the permission is granted
     */
    public boolean checkPermission(Context context, String name)
    {
        Permission perm = get(name);
        return perm != null && perm.checkPermission(context);
    }

    /**
     * Request a permission
     * @param activity Activity
     * @param name Name of the permission
     */
    public void requestPermission(Activity activity, String name)
    {
        if(containsKey(name))
        {
            ActivityCompat.requestPermissions(activity, new String[]{name}, Utils.REQUEST_PERMISSIONS);
        }
    }

    private List<String> getUnallowedPermissions(Activity activity)
    {
        List<String> list = new ArrayList<>();

        for (Object o : entrySet())
        {
            Entry pair = (Entry) o;
            Permission perm = (Permission) pair.getValue();
            if (!perm.checkPermission(activity))
            {
                list.add(perm.getLogName());
            }
        }

        return list;
    }

    /**
     * Check if all the permissions is granted
     * @param activity Activity
     * @param toast show or not a toast of unallowed permissions
     * @return
     */
    public boolean checkPermissions(Activity activity, Boolean toast)
    {
        List<String> list = getUnallowedPermissions(activity);

        if(list.size() == 1)
        {
            if(toast)
            {
                ToastFactory.createToast(activity, "Please enable this permission : " + list.get(0),
                        ToastFactory.ToastStyle.Info, Toast.LENGTH_SHORT);
            }
        }
        else if(list.size() > 0)
        {
            if(toast)
            {
                StringBuilder sb = new StringBuilder();
                for(int i = 0; i < list.size(); i++)
                {
                    if(i < list.size() - 1)
                    {
                        sb.append(list.get(i) + ", ");
                    }
                }
                ToastFactory.createToast(activity, "Please enable those permissions : " + sb.toString(),
                        ToastFactory.ToastStyle.Info, Toast.LENGTH_LONG);
            }
        }
        else { return true; }

        return false;
    }

    /**
     * Check all the permission without a toast
     * @param activity Activity
     * @return
     */
    public boolean checkPermissions(Activity activity)
    {
        return checkPermissions(activity, false);
    }

    /**
     * Request all the permissions
     * @param activity Activity
     */
    public void requestPermissions(Activity activity)
    {
        List<String> list = getUnallowedPermissions(activity);
        if(list.size() > 0)
        {
            String[] perms = list.toArray(new String[list.size()]);
            ActivityCompat.requestPermissions(activity, perms, Utils.REQUEST_PERMISSIONS);
        }
    }

    /**
     * A test to figure how we can detect if the user check never again when asking permissions
     * @param context Context
     * @param permission Name of the permission
     * @return
     */
    public boolean checkNeverAgain(Context context, String permission)
    {
        SharedPreferences genPrefs = context.getSharedPreferences("GENERIC_PREFERENCES", Context.MODE_PRIVATE);
        return genPrefs.getBoolean(permission, false);
    }
}
