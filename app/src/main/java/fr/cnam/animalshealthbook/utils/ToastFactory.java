package fr.cnam.animalshealthbook.utils;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import fr.cnam.animalshealthbook.R;

public class ToastFactory
{
    public enum ToastStyle
    {
        Error,
        Info,
        Success,
    }

    /**
     * Create a custom toast
     * @param activity Activity
     * @param message message to display
     * @param style Style  of the toast
     * @param length duration of the toast
     */
    public static void createToast(Activity activity, String message, ToastStyle style, int length) {
        Toast toast = null;
        switch (style) {
            case Error: {
                toast = createErrorToast(activity, message, length);
                break;
            }
            case Info: {
                toast = createInfoToast(activity, message, length);
                break;
            }
            case Success: {
                toast = createSuccessToast(activity, message, length);
                break;
            }
        }

        if(toast != null)
        {
            toast.show();
        }
    }

    private static Toast createToast(Activity activity, String message, int length,
                                     int textColor, int backgroundColor, int icon)
    {
        Toast toast = new Toast(activity);
        View view = activity.getLayoutInflater().inflate(R.layout.toast_layout, null, false);
        toast.setView(view);
        toast.setDuration(length);

        TextView textView = view.findViewById(R.id.toast_message);
        textView.setText(message);

        textView.setTextColor(textColor);

        toast.getView().setBackgroundResource(R.drawable.round_background);

        Drawable drawable = toast.getView().getBackground();


        if (drawable instanceof ShapeDrawable) {
            // cast to 'ShapeDrawable'
            ShapeDrawable shapeDrawable = (ShapeDrawable) drawable;
            shapeDrawable.getPaint().setColor(backgroundColor);
        } else if (drawable instanceof GradientDrawable) {
            // cast to 'GradientDrawable'
            GradientDrawable gradientDrawable = (GradientDrawable) drawable;
            gradientDrawable.setColor(backgroundColor);
        } else if (drawable instanceof ColorDrawable) {
            // alpha value may need to be set again after this call
            ColorDrawable colorDrawable = (ColorDrawable) drawable;
            colorDrawable.setColor(backgroundColor);
        }

        ImageView imageView = view.findViewById(R.id.toast_icon);
        imageView.setImageResource(icon);

        return toast;
    }

    private static Toast createErrorToast(Activity activity, String message, int length) {
        return createToast(activity, message, length,
                activity.getResources().getColor(R.color.white),
                activity.getResources().getColor(R.color.error),
                R.drawable.ic_warning_white_24dp);
    }

    private static Toast createInfoToast(Activity activity, String message, int length) {
        return createToast(activity, message, length,
                activity.getResources().getColor(R.color.white),
                activity.getResources().getColor(R.color.info),
                R.drawable.ic_info_white_24dp);
    }

    private static Toast createSuccessToast(Activity activity, String message, int length) {
        return createToast(activity, message, length,
                activity.getResources().getColor(R.color.white),
                activity.getResources().getColor(R.color.success),
                R.drawable.ic_check_white_24dp);

    }

}
